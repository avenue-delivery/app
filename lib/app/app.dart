import 'dart:io';

import 'package:avenue_delivery/ui/features/cart/overlay/cart_overlay_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:stacked/stacked.dart';

import '../generated/l10n.dart';
import '../navigation/app_router.gr.dart';

class App extends StatelessWidget {
  const App({super.key});

  static final router = AppRoutes();

  @override
  Widget build(BuildContext context) {
    const appName = 'Avenue Delivery';
    if (Platform.isAndroid) {
      return MaterialApp.router(
        title: appName,
        debugShowCheckedModeBanner: false,

        //navigation
        routerDelegate: router.delegate(),
        routeInformationParser: router.defaultRouteParser(),
        theme: Theme.of(context).copyWith(useMaterial3: true),

        //internalization
        supportedLocales: S.delegate.supportedLocales,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        builder: (context, child) {
          return Stack(
            children: [
              child ?? Container(),
              Positioned(
                bottom: 12 + MediaQuery.of(context).viewPadding.bottom,
                right: 0,
                child: const CartOverlayView(),
              ),
            ],
          );
        },
      );
    } else {
      return Theme(
        data: Theme.of(context).copyWith(useMaterial3: true),
        child: CupertinoApp.router(
          title: appName,
          debugShowCheckedModeBanner: false,

          //internalization
          supportedLocales: S.delegate.supportedLocales,
          localizationsDelegates: const [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],

          //navigation
          routerDelegate: router.delegate(),
          routeInformationParser: router.defaultRouteParser(),

          builder: (context, child) {
            return Stack(
              children: [
                child ?? Container(),
                Positioned(
                  bottom: 12 + MediaQuery.of(context).viewPadding.bottom,
                  right: 0,
                  child: const CartOverlayView(),
                ),
              ],
            );
          },
        ),
      );
    }
  }
}
