import 'package:avenue_delivery/generated/app/place.pbgrpc.dart';
import 'package:avenue_delivery/generated/common/empty.pb.dart';
import 'package:grpc/grpc.dart';

class PlacesRemoteDataSource {
  PlacesRemoteDataSource(this._clientChannel);
  final ClientChannel _clientChannel;
  late final PlaceServiceClient _placeServiceClient =
      PlaceServiceClient(_clientChannel);

  Future<PlacesDeliveryResponse> getAllDelivery() =>
      _placeServiceClient.getAllDeliveryPlaces(Empty());

  Future<PlacesServiceResponse> getAllServices() =>
      _placeServiceClient.getAllServicePlaces(Empty());

  Future<PlaceResponseCategories> getPlaceCategories(int placeId) =>
      _placeServiceClient.getPlaceCategoriesWithItems(
        PlaceRequest(
          id: placeId,
        ),
      );
}
