import 'package:avenue_delivery/data/places/remote/remote.dart';
import 'package:avenue_delivery/di/common/di_scope_widget.dart';
import 'package:avenue_delivery/domain/cart/cart_service.dart';
import 'package:avenue_delivery/domain/global/snackbar_service.dart';
import 'package:avenue_delivery/domain/places/places_service.dart';
import 'package:flutter/widgets.dart';
import 'package:grpc/grpc.dart';

class GlobalDependency extends AppAsyncDependency {
  late final SnackService snack;
  late final ClientChannel clientChannel;
  late final PlacesService placesService;
  late final CartService cartService;

  @override
  Future<void> initAsync(BuildContext context) async {
    snack = SnackService();

    clientChannel = ClientChannel(
      '158.160.15.88',
      port: 50051,
      options: const ChannelOptions(
        credentials: ChannelCredentials.insecure(),
      ),
    );

    final placesRemoteDS = PlacesRemoteDataSource(clientChannel);
    placesService = PlacesService(placesRemoteDS);
    cartService = CartService();
  }
}

extension DepContextExtension on BuildContext {
  GlobalDependency get global => read<GlobalDependency>();
}
