import 'package:avenue_delivery/generated/app/item.pb.dart';
import 'package:avenue_delivery/generated/app/order.pbgrpc.dart';
import 'package:stacked/stacked.dart';

class CartService with ReactiveServiceMixin {
  CartService() {
    listenToReactiveValues([
      items,
    ]);
  }

  ReactiveList<OrderItemRequest> items = ReactiveList<OrderItemRequest>();

  int inCart = 0;

  int itemsInCart(int id) {
    final index = items.indexWhere((p0) => p0.itemId == id);
    if (index == -1) {
      return 0;
    } else {
      return items[index].count;
    }
  }

  void addToCart(int id, int price) {
    final res = items.indexWhere((p0) => p0.itemId == id);
    if (res == -1) {
      items.add(
        OrderItemRequest(
          itemId: id,
          count: 1,
        ),
      );
    } else {
      items[res].count += 1;
    }
    inCart += price;
    notifyListeners();
  }

  void removeFromCart(int id, int price) {
    final res = items.indexWhere((p0) => p0.itemId == id);
    if (res == -1) {
      return;
    } else {
      if (items[res].count == 1) {
        items.removeAt(res);
      } else {
        items[res].count -= 1;
      }
    }
    inCart -= price;
    notifyListeners();
  }
}
