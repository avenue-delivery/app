import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import '../../app/app.dart';
import '../../res/assets/colors.gen.dart';

/// Глобальный сервис для показа снэкбаров(уведомлений)
///
/// [showInfo] - Показать обычный снэкбар
class SnackService {

   BuildContext get _rootContext => App.router.navigatorKey.currentContext!;

  Flushbar _infoSnack({
    required String title,
    required String message,
    FlushbarPosition position = FlushbarPosition.TOP,
  }) {
    return Flushbar(
      margin: const EdgeInsets.all(16),
      title: title,
      titleColor: ColorName.black,
      message: message,
      messageColor: ColorName.black,
      flushbarPosition: position,
      backgroundColor: ColorName.white.withOpacity(.75),
      barBlur: 10,
      borderRadius: BorderRadius.circular(8),
      duration: const Duration(seconds: 3),
    );
  }

  Future<void> showInfo({
    required String title,
    required String message,
    FlushbarPosition position = FlushbarPosition.TOP,
  }) {
    return _infoSnack(title: title, message: message, position: position)
        .show(_rootContext);
  }
}

