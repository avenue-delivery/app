import 'package:avenue_delivery/data/places/remote/remote.dart';
import 'package:avenue_delivery/generated/app/item_category.pb.dart';
import 'package:avenue_delivery/generated/app/place.pb.dart';
import 'package:stacked/stacked.dart';

class PlacesService with ReactiveServiceMixin {
  PlacesService(this.remote) {
    listenToReactiveValues([placesDelivery, placesService]);
  }
  final PlacesRemoteDataSource remote;

  final placesDelivery = ReactiveValue<PlacesDeliveryResponse?>(null);
  final placesService = ReactiveValue<PlacesServiceResponse?>(null);

  Future<void> fetchDelivery() async {
    placesDelivery.value = await remote.getAllDelivery();
  }

  Future<void> fetchService() async {
    placesService.value = await remote.getAllServices();
  }

  Future<List<ItemCategoryResponse>> fetchPlaceCategories(int placeId) async {
    final res = await remote.getPlaceCategories(placeId);
    return res.categories;
  }
}
