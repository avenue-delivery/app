///
//  Generated code. Do not modify.
//  source: admin/admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'admin.pb.dart' as $0;
export 'admin.pb.dart';

class AdminServiceClient extends $grpc.Client {
  static final _$login =
      $grpc.ClientMethod<$0.AdminLoginRequest, $0.AdminLoginResponse>(
          '/AdminService/Login',
          ($0.AdminLoginRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AdminLoginResponse.fromBuffer(value));

  AdminServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.AdminLoginResponse> login(
      $0.AdminLoginRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$login, request, options: options);
  }
}

abstract class AdminServiceBase extends $grpc.Service {
  $core.String get $name => 'AdminService';

  AdminServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.AdminLoginRequest, $0.AdminLoginResponse>(
        'Login',
        login_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AdminLoginRequest.fromBuffer(value),
        ($0.AdminLoginResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.AdminLoginResponse> login_Pre($grpc.ServiceCall call,
      $async.Future<$0.AdminLoginRequest> request) async {
    return login(call, await request);
  }

  $async.Future<$0.AdminLoginResponse> login(
      $grpc.ServiceCall call, $0.AdminLoginRequest request);
}
