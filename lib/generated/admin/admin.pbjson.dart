///
//  Generated code. Do not modify.
//  source: admin/admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use adminLoginRequestDescriptor instead')
const AdminLoginRequest$json = const {
  '1': 'AdminLoginRequest',
  '2': const [
    const {'1': 'login', '3': 1, '4': 1, '5': 9, '10': 'login'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `AdminLoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List adminLoginRequestDescriptor = $convert.base64Decode('ChFBZG1pbkxvZ2luUmVxdWVzdBIUCgVsb2dpbhgBIAEoCVIFbG9naW4SGgoIcGFzc3dvcmQYAiABKAlSCHBhc3N3b3Jk');
@$core.Deprecated('Use adminLoginResponseDescriptor instead')
const AdminLoginResponse$json = const {
  '1': 'AdminLoginResponse',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `AdminLoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List adminLoginResponseDescriptor = $convert.base64Decode('ChJBZG1pbkxvZ2luUmVzcG9uc2USFAoFdG9rZW4YASABKAlSBXRva2Vu');
