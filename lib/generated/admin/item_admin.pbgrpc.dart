///
//  Generated code. Do not modify.
//  source: admin/item_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'item_admin.pb.dart' as $0;
import '../common/empty.pb.dart' as $1;
export 'item_admin.pb.dart';

class ItemAdminServiceClient extends $grpc.Client {
  static final _$createItem =
      $grpc.ClientMethod<$0.CreateItemRequest, $1.Empty>(
          '/ItemAdminService/CreateItem',
          ($0.CreateItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$updateItem =
      $grpc.ClientMethod<$0.UpdateItemRequest, $1.Empty>(
          '/ItemAdminService/UpdateItem',
          ($0.UpdateItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$deleteItem =
      $grpc.ClientMethod<$0.DeleteItemRequest, $1.Empty>(
          '/ItemAdminService/DeleteItem',
          ($0.DeleteItemRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));

  ItemAdminServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.Empty> createItem($0.CreateItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createItem, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> updateItem($0.UpdateItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateItem, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> deleteItem($0.DeleteItemRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteItem, request, options: options);
  }
}

abstract class ItemAdminServiceBase extends $grpc.Service {
  $core.String get $name => 'ItemAdminService';

  ItemAdminServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CreateItemRequest, $1.Empty>(
        'CreateItem',
        createItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CreateItemRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateItemRequest, $1.Empty>(
        'UpdateItem',
        updateItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UpdateItemRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteItemRequest, $1.Empty>(
        'DeleteItem',
        deleteItem_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeleteItemRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> createItem_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateItemRequest> request) async {
    return createItem(call, await request);
  }

  $async.Future<$1.Empty> updateItem_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdateItemRequest> request) async {
    return updateItem(call, await request);
  }

  $async.Future<$1.Empty> deleteItem_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeleteItemRequest> request) async {
    return deleteItem(call, await request);
  }

  $async.Future<$1.Empty> createItem(
      $grpc.ServiceCall call, $0.CreateItemRequest request);
  $async.Future<$1.Empty> updateItem(
      $grpc.ServiceCall call, $0.UpdateItemRequest request);
  $async.Future<$1.Empty> deleteItem(
      $grpc.ServiceCall call, $0.DeleteItemRequest request);
}
