///
//  Generated code. Do not modify.
//  source: admin/item_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use createItemRequestDescriptor instead')
const CreateItemRequest$json = const {
  '1': 'CreateItemRequest',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'weight', '3': 4, '4': 1, '5': 5, '10': 'weight'},
    const {'1': 'price', '3': 5, '4': 1, '5': 5, '10': 'price'},
    const {'1': 'tags', '3': 6, '4': 3, '5': 9, '10': 'tags'},
    const {'1': 'categoryId', '3': 7, '4': 1, '5': 5, '10': 'categoryId'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `CreateItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createItemRequestDescriptor = $convert.base64Decode('ChFDcmVhdGVJdGVtUmVxdWVzdBIhCgVpbWFnZRgBIAEoCzILLkltYWdlTW9kZWxSBWltYWdlEhQKBXRpdGxlGAIgASgJUgV0aXRsZRIlCgtkZXNjcmlwdGlvbhgDIAEoCUgAUgtkZXNjcmlwdGlvbogBARIWCgZ3ZWlnaHQYBCABKAVSBndlaWdodBIUCgVwcmljZRgFIAEoBVIFcHJpY2USEgoEdGFncxgGIAMoCVIEdGFncxIeCgpjYXRlZ29yeUlkGAcgASgFUgpjYXRlZ29yeUlkQg4KDF9kZXNjcmlwdGlvbg==');
@$core.Deprecated('Use updateItemRequestDescriptor instead')
const UpdateItemRequest$json = const {
  '1': 'UpdateItemRequest',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'weight', '3': 4, '4': 1, '5': 5, '10': 'weight'},
    const {'1': 'price', '3': 5, '4': 1, '5': 5, '10': 'price'},
    const {'1': 'tags', '3': 6, '4': 3, '5': 9, '10': 'tags'},
    const {'1': 'categoryId', '3': 7, '4': 1, '5': 5, '10': 'categoryId'},
    const {'1': 'id', '3': 8, '4': 1, '5': 5, '10': 'id'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `UpdateItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateItemRequestDescriptor = $convert.base64Decode('ChFVcGRhdGVJdGVtUmVxdWVzdBIhCgVpbWFnZRgBIAEoCzILLkltYWdlTW9kZWxSBWltYWdlEhQKBXRpdGxlGAIgASgJUgV0aXRsZRIlCgtkZXNjcmlwdGlvbhgDIAEoCUgAUgtkZXNjcmlwdGlvbogBARIWCgZ3ZWlnaHQYBCABKAVSBndlaWdodBIUCgVwcmljZRgFIAEoBVIFcHJpY2USEgoEdGFncxgGIAMoCVIEdGFncxIeCgpjYXRlZ29yeUlkGAcgASgFUgpjYXRlZ29yeUlkEg4KAmlkGAggASgFUgJpZEIOCgxfZGVzY3JpcHRpb24=');
@$core.Deprecated('Use deleteItemRequestDescriptor instead')
const DeleteItemRequest$json = const {
  '1': 'DeleteItemRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `DeleteItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteItemRequestDescriptor = $convert.base64Decode('ChFEZWxldGVJdGVtUmVxdWVzdBIOCgJpZBgBIAEoBVICaWQ=');
