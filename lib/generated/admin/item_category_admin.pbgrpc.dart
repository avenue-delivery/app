///
//  Generated code. Do not modify.
//  source: admin/item_category_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'item_category_admin.pb.dart' as $0;
import '../common/empty.pb.dart' as $1;
export 'item_category_admin.pb.dart';

class ItemCategoryAdminServiceClient extends $grpc.Client {
  static final _$createCategory =
      $grpc.ClientMethod<$0.CreateCategoryRequest, $1.Empty>(
          '/ItemCategoryAdminService/CreateCategory',
          ($0.CreateCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$updateCategory =
      $grpc.ClientMethod<$0.UpdateCategoryRequest, $1.Empty>(
          '/ItemCategoryAdminService/UpdateCategory',
          ($0.UpdateCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$deleteCategory =
      $grpc.ClientMethod<$0.DeleteCategoryRequest, $1.Empty>(
          '/ItemCategoryAdminService/DeleteCategory',
          ($0.DeleteCategoryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));

  ItemCategoryAdminServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.Empty> createCategory(
      $0.CreateCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createCategory, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> updateCategory(
      $0.UpdateCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateCategory, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> deleteCategory(
      $0.DeleteCategoryRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteCategory, request, options: options);
  }
}

abstract class ItemCategoryAdminServiceBase extends $grpc.Service {
  $core.String get $name => 'ItemCategoryAdminService';

  ItemCategoryAdminServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CreateCategoryRequest, $1.Empty>(
        'CreateCategory',
        createCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreateCategoryRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateCategoryRequest, $1.Empty>(
        'UpdateCategory',
        updateCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.UpdateCategoryRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteCategoryRequest, $1.Empty>(
        'DeleteCategory',
        deleteCategory_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DeleteCategoryRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> createCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateCategoryRequest> request) async {
    return createCategory(call, await request);
  }

  $async.Future<$1.Empty> updateCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdateCategoryRequest> request) async {
    return updateCategory(call, await request);
  }

  $async.Future<$1.Empty> deleteCategory_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeleteCategoryRequest> request) async {
    return deleteCategory(call, await request);
  }

  $async.Future<$1.Empty> createCategory(
      $grpc.ServiceCall call, $0.CreateCategoryRequest request);
  $async.Future<$1.Empty> updateCategory(
      $grpc.ServiceCall call, $0.UpdateCategoryRequest request);
  $async.Future<$1.Empty> deleteCategory(
      $grpc.ServiceCall call, $0.DeleteCategoryRequest request);
}
