///
//  Generated code. Do not modify.
//  source: admin/item_category_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use createCategoryRequestDescriptor instead')
const CreateCategoryRequest$json = const {
  '1': 'CreateCategoryRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'placeId', '3': 2, '4': 1, '5': 5, '10': 'placeId'},
  ],
};

/// Descriptor for `CreateCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createCategoryRequestDescriptor = $convert.base64Decode('ChVDcmVhdGVDYXRlZ29yeVJlcXVlc3QSFAoFdGl0bGUYASABKAlSBXRpdGxlEhgKB3BsYWNlSWQYAiABKAVSB3BsYWNlSWQ=');
@$core.Deprecated('Use updateCategoryRequestDescriptor instead')
const UpdateCategoryRequest$json = const {
  '1': 'UpdateCategoryRequest',
  '2': const [
    const {'1': 'title', '3': 1, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'placeId', '3': 2, '4': 1, '5': 5, '10': 'placeId'},
    const {'1': 'categoryId', '3': 3, '4': 1, '5': 5, '10': 'categoryId'},
  ],
};

/// Descriptor for `UpdateCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateCategoryRequestDescriptor = $convert.base64Decode('ChVVcGRhdGVDYXRlZ29yeVJlcXVlc3QSFAoFdGl0bGUYASABKAlSBXRpdGxlEhgKB3BsYWNlSWQYAiABKAVSB3BsYWNlSWQSHgoKY2F0ZWdvcnlJZBgDIAEoBVIKY2F0ZWdvcnlJZA==');
@$core.Deprecated('Use deleteCategoryRequestDescriptor instead')
const DeleteCategoryRequest$json = const {
  '1': 'DeleteCategoryRequest',
  '2': const [
    const {'1': 'categoryId', '3': 1, '4': 1, '5': 5, '10': 'categoryId'},
  ],
};

/// Descriptor for `DeleteCategoryRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteCategoryRequestDescriptor = $convert.base64Decode('ChVEZWxldGVDYXRlZ29yeVJlcXVlc3QSHgoKY2F0ZWdvcnlJZBgBIAEoBVIKY2F0ZWdvcnlJZA==');
