///
//  Generated code. Do not modify.
//  source: admin/place_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'place_admin.pb.dart' as $0;
import '../common/empty.pb.dart' as $1;
export 'place_admin.pb.dart';

class PlaceAdminServiceClient extends $grpc.Client {
  static final _$createPlace =
      $grpc.ClientMethod<$0.CreatePlaceRequest, $1.Empty>(
          '/PlaceAdminService/CreatePlace',
          ($0.CreatePlaceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$updatePlace =
      $grpc.ClientMethod<$0.UpdatePlaceRequest, $1.Empty>(
          '/PlaceAdminService/UpdatePlace',
          ($0.UpdatePlaceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$deletePlace =
      $grpc.ClientMethod<$0.DeletePlaceRequest, $1.Empty>(
          '/PlaceAdminService/DeletePlace',
          ($0.DeletePlaceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));

  PlaceAdminServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.Empty> createPlace($0.CreatePlaceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createPlace, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> updatePlace($0.UpdatePlaceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updatePlace, request, options: options);
  }

  $grpc.ResponseFuture<$1.Empty> deletePlace($0.DeletePlaceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deletePlace, request, options: options);
  }
}

abstract class PlaceAdminServiceBase extends $grpc.Service {
  $core.String get $name => 'PlaceAdminService';

  PlaceAdminServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CreatePlaceRequest, $1.Empty>(
        'CreatePlace',
        createPlace_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreatePlaceRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdatePlaceRequest, $1.Empty>(
        'UpdatePlace',
        updatePlace_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.UpdatePlaceRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeletePlaceRequest, $1.Empty>(
        'DeletePlace',
        deletePlace_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.DeletePlaceRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> createPlace_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreatePlaceRequest> request) async {
    return createPlace(call, await request);
  }

  $async.Future<$1.Empty> updatePlace_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdatePlaceRequest> request) async {
    return updatePlace(call, await request);
  }

  $async.Future<$1.Empty> deletePlace_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeletePlaceRequest> request) async {
    return deletePlace(call, await request);
  }

  $async.Future<$1.Empty> createPlace(
      $grpc.ServiceCall call, $0.CreatePlaceRequest request);
  $async.Future<$1.Empty> updatePlace(
      $grpc.ServiceCall call, $0.UpdatePlaceRequest request);
  $async.Future<$1.Empty> deletePlace(
      $grpc.ServiceCall call, $0.DeletePlaceRequest request);
}
