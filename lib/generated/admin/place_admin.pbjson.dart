///
//  Generated code. Do not modify.
//  source: admin/place_admin.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use createPlaceRequestDescriptor instead')
const CreatePlaceRequest$json = const {
  '1': 'CreatePlaceRequest',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'deliveryCost', '3': 4, '4': 1, '5': 5, '10': 'deliveryCost'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `CreatePlaceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createPlaceRequestDescriptor = $convert.base64Decode('ChJDcmVhdGVQbGFjZVJlcXVlc3QSIQoFaW1hZ2UYASABKAsyCy5JbWFnZU1vZGVsUgVpbWFnZRIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSJQoLZGVzY3JpcHRpb24YAyABKAlIAFILZGVzY3JpcHRpb26IAQESIgoMZGVsaXZlcnlDb3N0GAQgASgFUgxkZWxpdmVyeUNvc3RCDgoMX2Rlc2NyaXB0aW9u');
@$core.Deprecated('Use updatePlaceRequestDescriptor instead')
const UpdatePlaceRequest$json = const {
  '1': 'UpdatePlaceRequest',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'deliveryCost', '3': 4, '4': 1, '5': 5, '10': 'deliveryCost'},
    const {'1': 'id', '3': 5, '4': 1, '5': 5, '10': 'id'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `UpdatePlaceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updatePlaceRequestDescriptor = $convert.base64Decode('ChJVcGRhdGVQbGFjZVJlcXVlc3QSIQoFaW1hZ2UYASABKAsyCy5JbWFnZU1vZGVsUgVpbWFnZRIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSJQoLZGVzY3JpcHRpb24YAyABKAlIAFILZGVzY3JpcHRpb26IAQESIgoMZGVsaXZlcnlDb3N0GAQgASgFUgxkZWxpdmVyeUNvc3QSDgoCaWQYBSABKAVSAmlkQg4KDF9kZXNjcmlwdGlvbg==');
@$core.Deprecated('Use deletePlaceRequestDescriptor instead')
const DeletePlaceRequest$json = const {
  '1': 'DeletePlaceRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `DeletePlaceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deletePlaceRequestDescriptor = $convert.base64Decode('ChJEZWxldGVQbGFjZVJlcXVlc3QSDgoCaWQYASABKAVSAmlk');
