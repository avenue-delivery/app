///
//  Generated code. Do not modify.
//  source: app/address.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class AddressRequestResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddressRequestResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'apartmentNumber', protoName: 'apartmentNumber')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'entrance')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'floor')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'comment')
    ..aOM<LatLng>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latLng', protoName: 'latLng', subBuilder: LatLng.create)
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address')
    ..hasRequiredFields = false
  ;

  AddressRequestResponse._() : super();
  factory AddressRequestResponse({
    $core.String? apartmentNumber,
    $core.String? entrance,
    $core.String? floor,
    $core.String? comment,
    LatLng? latLng,
    $core.String? address,
  }) {
    final _result = create();
    if (apartmentNumber != null) {
      _result.apartmentNumber = apartmentNumber;
    }
    if (entrance != null) {
      _result.entrance = entrance;
    }
    if (floor != null) {
      _result.floor = floor;
    }
    if (comment != null) {
      _result.comment = comment;
    }
    if (latLng != null) {
      _result.latLng = latLng;
    }
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory AddressRequestResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddressRequestResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddressRequestResponse clone() => AddressRequestResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddressRequestResponse copyWith(void Function(AddressRequestResponse) updates) => super.copyWith((message) => updates(message as AddressRequestResponse)) as AddressRequestResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddressRequestResponse create() => AddressRequestResponse._();
  AddressRequestResponse createEmptyInstance() => create();
  static $pb.PbList<AddressRequestResponse> createRepeated() => $pb.PbList<AddressRequestResponse>();
  @$core.pragma('dart2js:noInline')
  static AddressRequestResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddressRequestResponse>(create);
  static AddressRequestResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get apartmentNumber => $_getSZ(0);
  @$pb.TagNumber(1)
  set apartmentNumber($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasApartmentNumber() => $_has(0);
  @$pb.TagNumber(1)
  void clearApartmentNumber() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get entrance => $_getSZ(1);
  @$pb.TagNumber(2)
  set entrance($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEntrance() => $_has(1);
  @$pb.TagNumber(2)
  void clearEntrance() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get floor => $_getSZ(2);
  @$pb.TagNumber(3)
  set floor($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFloor() => $_has(2);
  @$pb.TagNumber(3)
  void clearFloor() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get comment => $_getSZ(3);
  @$pb.TagNumber(4)
  set comment($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasComment() => $_has(3);
  @$pb.TagNumber(4)
  void clearComment() => clearField(4);

  @$pb.TagNumber(5)
  LatLng get latLng => $_getN(4);
  @$pb.TagNumber(5)
  set latLng(LatLng v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasLatLng() => $_has(4);
  @$pb.TagNumber(5)
  void clearLatLng() => clearField(5);
  @$pb.TagNumber(5)
  LatLng ensureLatLng() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.String get address => $_getSZ(5);
  @$pb.TagNumber(6)
  set address($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAddress() => $_has(5);
  @$pb.TagNumber(6)
  void clearAddress() => clearField(6);
}

class LatLng extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LatLng', createEmptyInstance: create)
    ..a<$core.double>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'latitude', $pb.PbFieldType.OD)
    ..a<$core.double>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'longitude', $pb.PbFieldType.OD)
    ..hasRequiredFields = false
  ;

  LatLng._() : super();
  factory LatLng({
    $core.double? latitude,
    $core.double? longitude,
  }) {
    final _result = create();
    if (latitude != null) {
      _result.latitude = latitude;
    }
    if (longitude != null) {
      _result.longitude = longitude;
    }
    return _result;
  }
  factory LatLng.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LatLng.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LatLng clone() => LatLng()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LatLng copyWith(void Function(LatLng) updates) => super.copyWith((message) => updates(message as LatLng)) as LatLng; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LatLng create() => LatLng._();
  LatLng createEmptyInstance() => create();
  static $pb.PbList<LatLng> createRepeated() => $pb.PbList<LatLng>();
  @$core.pragma('dart2js:noInline')
  static LatLng getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LatLng>(create);
  static LatLng? _defaultInstance;

  @$pb.TagNumber(1)
  $core.double get latitude => $_getN(0);
  @$pb.TagNumber(1)
  set latitude($core.double v) { $_setDouble(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLatitude() => $_has(0);
  @$pb.TagNumber(1)
  void clearLatitude() => clearField(1);

  @$pb.TagNumber(2)
  $core.double get longitude => $_getN(1);
  @$pb.TagNumber(2)
  set longitude($core.double v) { $_setDouble(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLongitude() => $_has(1);
  @$pb.TagNumber(2)
  void clearLongitude() => clearField(2);
}

