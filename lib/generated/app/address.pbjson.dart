///
//  Generated code. Do not modify.
//  source: app/address.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use addressRequestResponseDescriptor instead')
const AddressRequestResponse$json = const {
  '1': 'AddressRequestResponse',
  '2': const [
    const {'1': 'apartmentNumber', '3': 1, '4': 1, '5': 9, '10': 'apartmentNumber'},
    const {'1': 'entrance', '3': 2, '4': 1, '5': 9, '10': 'entrance'},
    const {'1': 'floor', '3': 3, '4': 1, '5': 9, '10': 'floor'},
    const {'1': 'comment', '3': 4, '4': 1, '5': 9, '10': 'comment'},
    const {'1': 'latLng', '3': 5, '4': 1, '5': 11, '6': '.LatLng', '10': 'latLng'},
    const {'1': 'address', '3': 6, '4': 1, '5': 9, '10': 'address'},
  ],
};

/// Descriptor for `AddressRequestResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addressRequestResponseDescriptor = $convert.base64Decode('ChZBZGRyZXNzUmVxdWVzdFJlc3BvbnNlEigKD2FwYXJ0bWVudE51bWJlchgBIAEoCVIPYXBhcnRtZW50TnVtYmVyEhoKCGVudHJhbmNlGAIgASgJUghlbnRyYW5jZRIUCgVmbG9vchgDIAEoCVIFZmxvb3ISGAoHY29tbWVudBgEIAEoCVIHY29tbWVudBIfCgZsYXRMbmcYBSABKAsyBy5MYXRMbmdSBmxhdExuZxIYCgdhZGRyZXNzGAYgASgJUgdhZGRyZXNz');
@$core.Deprecated('Use latLngDescriptor instead')
const LatLng$json = const {
  '1': 'LatLng',
  '2': const [
    const {'1': 'latitude', '3': 1, '4': 1, '5': 1, '10': 'latitude'},
    const {'1': 'longitude', '3': 2, '4': 1, '5': 1, '10': 'longitude'},
  ],
};

/// Descriptor for `LatLng`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List latLngDescriptor = $convert.base64Decode('CgZMYXRMbmcSGgoIbGF0aXR1ZGUYASABKAFSCGxhdGl0dWRlEhwKCWxvbmdpdHVkZRgCIAEoAVIJbG9uZ2l0dWRl');
