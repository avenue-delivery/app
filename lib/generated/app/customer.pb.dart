///
//  Generated code. Do not modify.
//  source: app/customer.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'address.pb.dart' as $2;

class CustomerCodeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomerCodeRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..hasRequiredFields = false
  ;

  CustomerCodeRequest._() : super();
  factory CustomerCodeRequest({
    $core.String? email,
  }) {
    final _result = create();
    if (email != null) {
      _result.email = email;
    }
    return _result;
  }
  factory CustomerCodeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomerCodeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomerCodeRequest clone() => CustomerCodeRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomerCodeRequest copyWith(void Function(CustomerCodeRequest) updates) => super.copyWith((message) => updates(message as CustomerCodeRequest)) as CustomerCodeRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomerCodeRequest create() => CustomerCodeRequest._();
  CustomerCodeRequest createEmptyInstance() => create();
  static $pb.PbList<CustomerCodeRequest> createRepeated() => $pb.PbList<CustomerCodeRequest>();
  @$core.pragma('dart2js:noInline')
  static CustomerCodeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomerCodeRequest>(create);
  static CustomerCodeRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);
}

class CustomerAuthRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomerAuthRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'code')
    ..hasRequiredFields = false
  ;

  CustomerAuthRequest._() : super();
  factory CustomerAuthRequest({
    $core.String? email,
    $core.String? code,
  }) {
    final _result = create();
    if (email != null) {
      _result.email = email;
    }
    if (code != null) {
      _result.code = code;
    }
    return _result;
  }
  factory CustomerAuthRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomerAuthRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomerAuthRequest clone() => CustomerAuthRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomerAuthRequest copyWith(void Function(CustomerAuthRequest) updates) => super.copyWith((message) => updates(message as CustomerAuthRequest)) as CustomerAuthRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomerAuthRequest create() => CustomerAuthRequest._();
  CustomerAuthRequest createEmptyInstance() => create();
  static $pb.PbList<CustomerAuthRequest> createRepeated() => $pb.PbList<CustomerAuthRequest>();
  @$core.pragma('dart2js:noInline')
  static CustomerAuthRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomerAuthRequest>(create);
  static CustomerAuthRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get code => $_getSZ(1);
  @$pb.TagNumber(2)
  set code($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCode() => $_has(1);
  @$pb.TagNumber(2)
  void clearCode() => clearField(2);
}

class CustomerPersonalDataRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomerPersonalDataRequest', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fio')
    ..aOM<$2.AddressRequestResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address', subBuilder: $2.AddressRequestResponse.create)
    ..hasRequiredFields = false
  ;

  CustomerPersonalDataRequest._() : super();
  factory CustomerPersonalDataRequest({
    $core.String? fio,
    $2.AddressRequestResponse? address,
  }) {
    final _result = create();
    if (fio != null) {
      _result.fio = fio;
    }
    if (address != null) {
      _result.address = address;
    }
    return _result;
  }
  factory CustomerPersonalDataRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomerPersonalDataRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomerPersonalDataRequest clone() => CustomerPersonalDataRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomerPersonalDataRequest copyWith(void Function(CustomerPersonalDataRequest) updates) => super.copyWith((message) => updates(message as CustomerPersonalDataRequest)) as CustomerPersonalDataRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomerPersonalDataRequest create() => CustomerPersonalDataRequest._();
  CustomerPersonalDataRequest createEmptyInstance() => create();
  static $pb.PbList<CustomerPersonalDataRequest> createRepeated() => $pb.PbList<CustomerPersonalDataRequest>();
  @$core.pragma('dart2js:noInline')
  static CustomerPersonalDataRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomerPersonalDataRequest>(create);
  static CustomerPersonalDataRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fio => $_getSZ(0);
  @$pb.TagNumber(1)
  set fio($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFio() => $_has(0);
  @$pb.TagNumber(1)
  void clearFio() => clearField(1);

  @$pb.TagNumber(2)
  $2.AddressRequestResponse get address => $_getN(1);
  @$pb.TagNumber(2)
  set address($2.AddressRequestResponse v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearAddress() => clearField(2);
  @$pb.TagNumber(2)
  $2.AddressRequestResponse ensureAddress() => $_ensure(1);
}

class CustomerResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomerResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'email')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'fio')
    ..aOM<$2.AddressRequestResponse>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'address', subBuilder: $2.AddressRequestResponse.create)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..hasRequiredFields = false
  ;

  CustomerResponse._() : super();
  factory CustomerResponse({
    $core.String? email,
    $core.String? fio,
    $2.AddressRequestResponse? address,
    $core.String? phone,
  }) {
    final _result = create();
    if (email != null) {
      _result.email = email;
    }
    if (fio != null) {
      _result.fio = fio;
    }
    if (address != null) {
      _result.address = address;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    return _result;
  }
  factory CustomerResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomerResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomerResponse clone() => CustomerResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomerResponse copyWith(void Function(CustomerResponse) updates) => super.copyWith((message) => updates(message as CustomerResponse)) as CustomerResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomerResponse create() => CustomerResponse._();
  CustomerResponse createEmptyInstance() => create();
  static $pb.PbList<CustomerResponse> createRepeated() => $pb.PbList<CustomerResponse>();
  @$core.pragma('dart2js:noInline')
  static CustomerResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomerResponse>(create);
  static CustomerResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get fio => $_getSZ(1);
  @$pb.TagNumber(2)
  set fio($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFio() => $_has(1);
  @$pb.TagNumber(2)
  void clearFio() => clearField(2);

  @$pb.TagNumber(3)
  $2.AddressRequestResponse get address => $_getN(2);
  @$pb.TagNumber(3)
  set address($2.AddressRequestResponse v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasAddress() => $_has(2);
  @$pb.TagNumber(3)
  void clearAddress() => clearField(3);
  @$pb.TagNumber(3)
  $2.AddressRequestResponse ensureAddress() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get phone => $_getSZ(3);
  @$pb.TagNumber(4)
  set phone($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPhone() => $_has(3);
  @$pb.TagNumber(4)
  void clearPhone() => clearField(4);
}

class CustomerTokenResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CustomerTokenResponse', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..hasRequiredFields = false
  ;

  CustomerTokenResponse._() : super();
  factory CustomerTokenResponse({
    $core.String? token,
  }) {
    final _result = create();
    if (token != null) {
      _result.token = token;
    }
    return _result;
  }
  factory CustomerTokenResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CustomerTokenResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CustomerTokenResponse clone() => CustomerTokenResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CustomerTokenResponse copyWith(void Function(CustomerTokenResponse) updates) => super.copyWith((message) => updates(message as CustomerTokenResponse)) as CustomerTokenResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CustomerTokenResponse create() => CustomerTokenResponse._();
  CustomerTokenResponse createEmptyInstance() => create();
  static $pb.PbList<CustomerTokenResponse> createRepeated() => $pb.PbList<CustomerTokenResponse>();
  @$core.pragma('dart2js:noInline')
  static CustomerTokenResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CustomerTokenResponse>(create);
  static CustomerTokenResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);
}

