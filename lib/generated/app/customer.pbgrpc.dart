///
//  Generated code. Do not modify.
//  source: app/customer.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'customer.pb.dart' as $0;
import '../common/empty.pb.dart' as $1;
export 'customer.pb.dart';

class CustomerServiceClient extends $grpc.Client {
  static final _$requestCode =
      $grpc.ClientMethod<$0.CustomerCodeRequest, $1.Empty>(
          '/CustomerService/RequestCode',
          ($0.CustomerCodeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$authorizeViaCode =
      $grpc.ClientMethod<$0.CustomerAuthRequest, $0.CustomerTokenResponse>(
          '/CustomerService/AuthorizeViaCode',
          ($0.CustomerAuthRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CustomerTokenResponse.fromBuffer(value));
  static final _$addPersonalData =
      $grpc.ClientMethod<$0.CustomerPersonalDataRequest, $0.CustomerResponse>(
          '/CustomerService/AddPersonalData',
          ($0.CustomerPersonalDataRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CustomerResponse.fromBuffer(value));

  CustomerServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.Empty> requestCode($0.CustomerCodeRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$requestCode, request, options: options);
  }

  $grpc.ResponseFuture<$0.CustomerTokenResponse> authorizeViaCode(
      $0.CustomerAuthRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$authorizeViaCode, request, options: options);
  }

  $grpc.ResponseFuture<$0.CustomerResponse> addPersonalData(
      $0.CustomerPersonalDataRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addPersonalData, request, options: options);
  }
}

abstract class CustomerServiceBase extends $grpc.Service {
  $core.String get $name => 'CustomerService';

  CustomerServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CustomerCodeRequest, $1.Empty>(
        'RequestCode',
        requestCode_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CustomerCodeRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.CustomerAuthRequest, $0.CustomerTokenResponse>(
            'AuthorizeViaCode',
            authorizeViaCode_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.CustomerAuthRequest.fromBuffer(value),
            ($0.CustomerTokenResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CustomerPersonalDataRequest,
            $0.CustomerResponse>(
        'AddPersonalData',
        addPersonalData_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CustomerPersonalDataRequest.fromBuffer(value),
        ($0.CustomerResponse value) => value.writeToBuffer()));
  }

  $async.Future<$1.Empty> requestCode_Pre($grpc.ServiceCall call,
      $async.Future<$0.CustomerCodeRequest> request) async {
    return requestCode(call, await request);
  }

  $async.Future<$0.CustomerTokenResponse> authorizeViaCode_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.CustomerAuthRequest> request) async {
    return authorizeViaCode(call, await request);
  }

  $async.Future<$0.CustomerResponse> addPersonalData_Pre($grpc.ServiceCall call,
      $async.Future<$0.CustomerPersonalDataRequest> request) async {
    return addPersonalData(call, await request);
  }

  $async.Future<$1.Empty> requestCode(
      $grpc.ServiceCall call, $0.CustomerCodeRequest request);
  $async.Future<$0.CustomerTokenResponse> authorizeViaCode(
      $grpc.ServiceCall call, $0.CustomerAuthRequest request);
  $async.Future<$0.CustomerResponse> addPersonalData(
      $grpc.ServiceCall call, $0.CustomerPersonalDataRequest request);
}
