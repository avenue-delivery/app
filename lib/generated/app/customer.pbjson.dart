///
//  Generated code. Do not modify.
//  source: app/customer.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use customerCodeRequestDescriptor instead')
const CustomerCodeRequest$json = const {
  '1': 'CustomerCodeRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
  ],
};

/// Descriptor for `CustomerCodeRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customerCodeRequestDescriptor = $convert.base64Decode('ChNDdXN0b21lckNvZGVSZXF1ZXN0EhQKBWVtYWlsGAEgASgJUgVlbWFpbA==');
@$core.Deprecated('Use customerAuthRequestDescriptor instead')
const CustomerAuthRequest$json = const {
  '1': 'CustomerAuthRequest',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'code', '3': 2, '4': 1, '5': 9, '10': 'code'},
  ],
};

/// Descriptor for `CustomerAuthRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customerAuthRequestDescriptor = $convert.base64Decode('ChNDdXN0b21lckF1dGhSZXF1ZXN0EhQKBWVtYWlsGAEgASgJUgVlbWFpbBISCgRjb2RlGAIgASgJUgRjb2Rl');
@$core.Deprecated('Use customerPersonalDataRequestDescriptor instead')
const CustomerPersonalDataRequest$json = const {
  '1': 'CustomerPersonalDataRequest',
  '2': const [
    const {'1': 'fio', '3': 1, '4': 1, '5': 9, '10': 'fio'},
    const {'1': 'address', '3': 2, '4': 1, '5': 11, '6': '.AddressRequestResponse', '10': 'address'},
  ],
};

/// Descriptor for `CustomerPersonalDataRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customerPersonalDataRequestDescriptor = $convert.base64Decode('ChtDdXN0b21lclBlcnNvbmFsRGF0YVJlcXVlc3QSEAoDZmlvGAEgASgJUgNmaW8SMQoHYWRkcmVzcxgCIAEoCzIXLkFkZHJlc3NSZXF1ZXN0UmVzcG9uc2VSB2FkZHJlc3M=');
@$core.Deprecated('Use customerResponseDescriptor instead')
const CustomerResponse$json = const {
  '1': 'CustomerResponse',
  '2': const [
    const {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    const {'1': 'fio', '3': 2, '4': 1, '5': 9, '9': 0, '10': 'fio', '17': true},
    const {'1': 'address', '3': 3, '4': 1, '5': 11, '6': '.AddressRequestResponse', '9': 1, '10': 'address', '17': true},
    const {'1': 'phone', '3': 4, '4': 1, '5': 9, '9': 2, '10': 'phone', '17': true},
  ],
  '8': const [
    const {'1': '_fio'},
    const {'1': '_address'},
    const {'1': '_phone'},
  ],
};

/// Descriptor for `CustomerResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customerResponseDescriptor = $convert.base64Decode('ChBDdXN0b21lclJlc3BvbnNlEhQKBWVtYWlsGAEgASgJUgVlbWFpbBIVCgNmaW8YAiABKAlIAFIDZmlviAEBEjYKB2FkZHJlc3MYAyABKAsyFy5BZGRyZXNzUmVxdWVzdFJlc3BvbnNlSAFSB2FkZHJlc3OIAQESGQoFcGhvbmUYBCABKAlIAlIFcGhvbmWIAQFCBgoEX2Zpb0IKCghfYWRkcmVzc0IICgZfcGhvbmU=');
@$core.Deprecated('Use customerTokenResponseDescriptor instead')
const CustomerTokenResponse$json = const {
  '1': 'CustomerTokenResponse',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `CustomerTokenResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List customerTokenResponseDescriptor = $convert.base64Decode('ChVDdXN0b21lclRva2VuUmVzcG9uc2USFAoFdG9rZW4YASABKAlSBXRva2Vu');
