///
//  Generated code. Do not modify.
//  source: app/item.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use itemResponseDescriptor instead')
const ItemResponse$json = const {
  '1': 'ItemResponse',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'weight', '3': 4, '4': 1, '5': 5, '10': 'weight'},
    const {'1': 'price', '3': 5, '4': 1, '5': 5, '10': 'price'},
    const {'1': 'tags', '3': 6, '4': 3, '5': 9, '10': 'tags'},
    const {'1': 'id', '3': 7, '4': 1, '5': 5, '10': 'id'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `ItemResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List itemResponseDescriptor = $convert.base64Decode('CgxJdGVtUmVzcG9uc2USIQoFaW1hZ2UYASABKAsyCy5JbWFnZU1vZGVsUgVpbWFnZRIUCgV0aXRsZRgCIAEoCVIFdGl0bGUSJQoLZGVzY3JpcHRpb24YAyABKAlIAFILZGVzY3JpcHRpb26IAQESFgoGd2VpZ2h0GAQgASgFUgZ3ZWlnaHQSFAoFcHJpY2UYBSABKAVSBXByaWNlEhIKBHRhZ3MYBiADKAlSBHRhZ3MSDgoCaWQYByABKAVSAmlkQg4KDF9kZXNjcmlwdGlvbg==');
