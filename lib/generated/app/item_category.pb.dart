///
//  Generated code. Do not modify.
//  source: app/item_category.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'item.pb.dart' as $0;

class ItemCategoryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ItemCategoryResponse', createEmptyInstance: create)
    ..pc<$0.ItemResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: $0.ItemResponse.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..hasRequiredFields = false
  ;

  ItemCategoryResponse._() : super();
  factory ItemCategoryResponse({
    $core.Iterable<$0.ItemResponse>? items,
    $core.String? title,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    if (title != null) {
      _result.title = title;
    }
    return _result;
  }
  factory ItemCategoryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ItemCategoryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ItemCategoryResponse clone() => ItemCategoryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ItemCategoryResponse copyWith(void Function(ItemCategoryResponse) updates) => super.copyWith((message) => updates(message as ItemCategoryResponse)) as ItemCategoryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ItemCategoryResponse create() => ItemCategoryResponse._();
  ItemCategoryResponse createEmptyInstance() => create();
  static $pb.PbList<ItemCategoryResponse> createRepeated() => $pb.PbList<ItemCategoryResponse>();
  @$core.pragma('dart2js:noInline')
  static ItemCategoryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ItemCategoryResponse>(create);
  static ItemCategoryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$0.ItemResponse> get items => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);
}

