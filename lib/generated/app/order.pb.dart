///
//  Generated code. Do not modify.
//  source: app/order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'item.pb.dart' as $2;

import 'order.pbenum.dart';

export 'order.pbenum.dart';

class OrderRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderRequest', createEmptyInstance: create)
    ..pc<OrderItemRequest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: OrderItemRequest.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'customerId', $pb.PbFieldType.O3, protoName: 'customerId')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'placeId', $pb.PbFieldType.O3, protoName: 'placeId')
    ..e<OrderPaymentType>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paymentType', $pb.PbFieldType.OE, protoName: 'paymentType', defaultOrMaker: OrderPaymentType.CASH, valueOf: OrderPaymentType.valueOf, enumValues: OrderPaymentType.values)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'phone')
    ..hasRequiredFields = false
  ;

  OrderRequest._() : super();
  factory OrderRequest({
    $core.Iterable<OrderItemRequest>? items,
    $core.int? customerId,
    $core.int? placeId,
    OrderPaymentType? paymentType,
    $core.String? phone,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    if (customerId != null) {
      _result.customerId = customerId;
    }
    if (placeId != null) {
      _result.placeId = placeId;
    }
    if (paymentType != null) {
      _result.paymentType = paymentType;
    }
    if (phone != null) {
      _result.phone = phone;
    }
    return _result;
  }
  factory OrderRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderRequest clone() => OrderRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderRequest copyWith(void Function(OrderRequest) updates) => super.copyWith((message) => updates(message as OrderRequest)) as OrderRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderRequest create() => OrderRequest._();
  OrderRequest createEmptyInstance() => create();
  static $pb.PbList<OrderRequest> createRepeated() => $pb.PbList<OrderRequest>();
  @$core.pragma('dart2js:noInline')
  static OrderRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderRequest>(create);
  static OrderRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderItemRequest> get items => $_getList(0);

  @$pb.TagNumber(2)
  $core.int get customerId => $_getIZ(1);
  @$pb.TagNumber(2)
  set customerId($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCustomerId() => $_has(1);
  @$pb.TagNumber(2)
  void clearCustomerId() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get placeId => $_getIZ(2);
  @$pb.TagNumber(3)
  set placeId($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPlaceId() => $_has(2);
  @$pb.TagNumber(3)
  void clearPlaceId() => clearField(3);

  @$pb.TagNumber(4)
  OrderPaymentType get paymentType => $_getN(3);
  @$pb.TagNumber(4)
  set paymentType(OrderPaymentType v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasPaymentType() => $_has(3);
  @$pb.TagNumber(4)
  void clearPaymentType() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get phone => $_getSZ(4);
  @$pb.TagNumber(5)
  set phone($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPhone() => $_has(4);
  @$pb.TagNumber(5)
  void clearPhone() => clearField(5);
}

class AllOrdersResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AllOrdersResponse', createEmptyInstance: create)
    ..pc<OrderRequest>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'oorders', $pb.PbFieldType.PM, subBuilder: OrderRequest.create)
    ..hasRequiredFields = false
  ;

  AllOrdersResponse._() : super();
  factory AllOrdersResponse({
    $core.Iterable<OrderRequest>? oorders,
  }) {
    final _result = create();
    if (oorders != null) {
      _result.oorders.addAll(oorders);
    }
    return _result;
  }
  factory AllOrdersResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AllOrdersResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AllOrdersResponse clone() => AllOrdersResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AllOrdersResponse copyWith(void Function(AllOrdersResponse) updates) => super.copyWith((message) => updates(message as AllOrdersResponse)) as AllOrdersResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AllOrdersResponse create() => AllOrdersResponse._();
  AllOrdersResponse createEmptyInstance() => create();
  static $pb.PbList<AllOrdersResponse> createRepeated() => $pb.PbList<AllOrdersResponse>();
  @$core.pragma('dart2js:noInline')
  static AllOrdersResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AllOrdersResponse>(create);
  static AllOrdersResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderRequest> get oorders => $_getList(0);
}

class OrderResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderResponse', createEmptyInstance: create)
    ..pc<OrderItemResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: OrderItemResponse.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'createdAt', $pb.PbFieldType.O3, protoName: 'createdAt')
    ..e<OrderPaymentType>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'paymentType', $pb.PbFieldType.OE, protoName: 'paymentType', defaultOrMaker: OrderPaymentType.CASH, valueOf: OrderPaymentType.valueOf, enumValues: OrderPaymentType.values)
    ..e<OrderStatus>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: OrderStatus.PAYMENT_PROCESSING, valueOf: OrderStatus.valueOf, enumValues: OrderStatus.values)
    ..hasRequiredFields = false
  ;

  OrderResponse._() : super();
  factory OrderResponse({
    $core.Iterable<OrderItemResponse>? items,
    $core.int? createdAt,
    OrderPaymentType? paymentType,
    OrderStatus? status,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    if (createdAt != null) {
      _result.createdAt = createdAt;
    }
    if (paymentType != null) {
      _result.paymentType = paymentType;
    }
    if (status != null) {
      _result.status = status;
    }
    return _result;
  }
  factory OrderResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderResponse clone() => OrderResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderResponse copyWith(void Function(OrderResponse) updates) => super.copyWith((message) => updates(message as OrderResponse)) as OrderResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderResponse create() => OrderResponse._();
  OrderResponse createEmptyInstance() => create();
  static $pb.PbList<OrderResponse> createRepeated() => $pb.PbList<OrderResponse>();
  @$core.pragma('dart2js:noInline')
  static OrderResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderResponse>(create);
  static OrderResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<OrderItemResponse> get items => $_getList(0);

  @$pb.TagNumber(2)
  $core.int get createdAt => $_getIZ(1);
  @$pb.TagNumber(2)
  set createdAt($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCreatedAt() => $_has(1);
  @$pb.TagNumber(2)
  void clearCreatedAt() => clearField(2);

  @$pb.TagNumber(3)
  OrderPaymentType get paymentType => $_getN(2);
  @$pb.TagNumber(3)
  set paymentType(OrderPaymentType v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasPaymentType() => $_has(2);
  @$pb.TagNumber(3)
  void clearPaymentType() => clearField(3);

  @$pb.TagNumber(4)
  OrderStatus get status => $_getN(3);
  @$pb.TagNumber(4)
  set status(OrderStatus v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasStatus() => $_has(3);
  @$pb.TagNumber(4)
  void clearStatus() => clearField(4);
}

class OrderItemRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderItemRequest', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'itemId', $pb.PbFieldType.O3, protoName: 'itemId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'count', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  OrderItemRequest._() : super();
  factory OrderItemRequest({
    $core.int? itemId,
    $core.int? count,
  }) {
    final _result = create();
    if (itemId != null) {
      _result.itemId = itemId;
    }
    if (count != null) {
      _result.count = count;
    }
    return _result;
  }
  factory OrderItemRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderItemRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderItemRequest clone() => OrderItemRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderItemRequest copyWith(void Function(OrderItemRequest) updates) => super.copyWith((message) => updates(message as OrderItemRequest)) as OrderItemRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderItemRequest create() => OrderItemRequest._();
  OrderItemRequest createEmptyInstance() => create();
  static $pb.PbList<OrderItemRequest> createRepeated() => $pb.PbList<OrderItemRequest>();
  @$core.pragma('dart2js:noInline')
  static OrderItemRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderItemRequest>(create);
  static OrderItemRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get itemId => $_getIZ(0);
  @$pb.TagNumber(1)
  set itemId($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasItemId() => $_has(0);
  @$pb.TagNumber(1)
  void clearItemId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get count => $_getIZ(1);
  @$pb.TagNumber(2)
  set count($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearCount() => clearField(2);
}

class OrderItemResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OrderItemResponse', createEmptyInstance: create)
    ..aOM<$2.ItemResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'item', subBuilder: $2.ItemResponse.create)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'count', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  OrderItemResponse._() : super();
  factory OrderItemResponse({
    $2.ItemResponse? item,
    $core.int? count,
  }) {
    final _result = create();
    if (item != null) {
      _result.item = item;
    }
    if (count != null) {
      _result.count = count;
    }
    return _result;
  }
  factory OrderItemResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OrderItemResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OrderItemResponse clone() => OrderItemResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OrderItemResponse copyWith(void Function(OrderItemResponse) updates) => super.copyWith((message) => updates(message as OrderItemResponse)) as OrderItemResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OrderItemResponse create() => OrderItemResponse._();
  OrderItemResponse createEmptyInstance() => create();
  static $pb.PbList<OrderItemResponse> createRepeated() => $pb.PbList<OrderItemResponse>();
  @$core.pragma('dart2js:noInline')
  static OrderItemResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OrderItemResponse>(create);
  static OrderItemResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $2.ItemResponse get item => $_getN(0);
  @$pb.TagNumber(1)
  set item($2.ItemResponse v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasItem() => $_has(0);
  @$pb.TagNumber(1)
  void clearItem() => clearField(1);
  @$pb.TagNumber(1)
  $2.ItemResponse ensureItem() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get count => $_getIZ(1);
  @$pb.TagNumber(2)
  set count($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCount() => $_has(1);
  @$pb.TagNumber(2)
  void clearCount() => clearField(2);
}

