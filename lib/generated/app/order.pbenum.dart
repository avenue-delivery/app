///
//  Generated code. Do not modify.
//  source: app/order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class OrderPaymentType extends $pb.ProtobufEnum {
  static const OrderPaymentType CASH = OrderPaymentType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CASH');
  static const OrderPaymentType CARD = OrderPaymentType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CARD');

  static const $core.List<OrderPaymentType> values = <OrderPaymentType> [
    CASH,
    CARD,
  ];

  static final $core.Map<$core.int, OrderPaymentType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static OrderPaymentType? valueOf($core.int value) => _byValue[value];

  const OrderPaymentType._($core.int v, $core.String n) : super(v, n);
}

class OrderStatus extends $pb.ProtobufEnum {
  static const OrderStatus PAYMENT_PROCESSING = OrderStatus._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PAYMENT_PROCESSING');
  static const OrderStatus PAID = OrderStatus._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PAID');
  static const OrderStatus CANCELED = OrderStatus._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CANCELED');
  static const OrderStatus IN_DELIVERY = OrderStatus._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'IN_DELIVERY');
  static const OrderStatus FINISHED = OrderStatus._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'FINISHED');

  static const $core.List<OrderStatus> values = <OrderStatus> [
    PAYMENT_PROCESSING,
    PAID,
    CANCELED,
    IN_DELIVERY,
    FINISHED,
  ];

  static final $core.Map<$core.int, OrderStatus> _byValue = $pb.ProtobufEnum.initByValue(values);
  static OrderStatus? valueOf($core.int value) => _byValue[value];

  const OrderStatus._($core.int v, $core.String n) : super(v, n);
}

