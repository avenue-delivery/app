///
//  Generated code. Do not modify.
//  source: app/order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'order.pb.dart' as $0;
import '../common/empty.pb.dart' as $1;
export 'order.pb.dart';

class OrderServiceClient extends $grpc.Client {
  static final _$createOrder =
      $grpc.ClientMethod<$0.OrderRequest, $0.OrderResponse>(
          '/OrderService/CreateOrder',
          ($0.OrderRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.OrderResponse.fromBuffer(value));
  static final _$getMyOrders =
      $grpc.ClientMethod<$1.Empty, $0.AllOrdersResponse>(
          '/OrderService/GetMyOrders',
          ($1.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AllOrdersResponse.fromBuffer(value));

  OrderServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.OrderResponse> createOrder($0.OrderRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createOrder, request, options: options);
  }

  $grpc.ResponseFuture<$0.AllOrdersResponse> getMyOrders($1.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getMyOrders, request, options: options);
  }
}

abstract class OrderServiceBase extends $grpc.Service {
  $core.String get $name => 'OrderService';

  OrderServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.OrderRequest, $0.OrderResponse>(
        'CreateOrder',
        createOrder_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.OrderRequest.fromBuffer(value),
        ($0.OrderResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Empty, $0.AllOrdersResponse>(
        'GetMyOrders',
        getMyOrders_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Empty.fromBuffer(value),
        ($0.AllOrdersResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.OrderResponse> createOrder_Pre(
      $grpc.ServiceCall call, $async.Future<$0.OrderRequest> request) async {
    return createOrder(call, await request);
  }

  $async.Future<$0.AllOrdersResponse> getMyOrders_Pre(
      $grpc.ServiceCall call, $async.Future<$1.Empty> request) async {
    return getMyOrders(call, await request);
  }

  $async.Future<$0.OrderResponse> createOrder(
      $grpc.ServiceCall call, $0.OrderRequest request);
  $async.Future<$0.AllOrdersResponse> getMyOrders(
      $grpc.ServiceCall call, $1.Empty request);
}
