///
//  Generated code. Do not modify.
//  source: app/order.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use orderPaymentTypeDescriptor instead')
const OrderPaymentType$json = const {
  '1': 'OrderPaymentType',
  '2': const [
    const {'1': 'CASH', '2': 0},
    const {'1': 'CARD', '2': 1},
  ],
};

/// Descriptor for `OrderPaymentType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List orderPaymentTypeDescriptor = $convert.base64Decode('ChBPcmRlclBheW1lbnRUeXBlEggKBENBU0gQABIICgRDQVJEEAE=');
@$core.Deprecated('Use orderStatusDescriptor instead')
const OrderStatus$json = const {
  '1': 'OrderStatus',
  '2': const [
    const {'1': 'PAYMENT_PROCESSING', '2': 0},
    const {'1': 'PAID', '2': 1},
    const {'1': 'CANCELED', '2': 2},
    const {'1': 'IN_DELIVERY', '2': 3},
    const {'1': 'FINISHED', '2': 4},
  ],
};

/// Descriptor for `OrderStatus`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List orderStatusDescriptor = $convert.base64Decode('CgtPcmRlclN0YXR1cxIWChJQQVlNRU5UX1BST0NFU1NJTkcQABIICgRQQUlEEAESDAoIQ0FOQ0VMRUQQAhIPCgtJTl9ERUxJVkVSWRADEgwKCEZJTklTSEVEEAQ=');
@$core.Deprecated('Use orderRequestDescriptor instead')
const OrderRequest$json = const {
  '1': 'OrderRequest',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.OrderItemRequest', '10': 'items'},
    const {'1': 'customerId', '3': 2, '4': 1, '5': 5, '10': 'customerId'},
    const {'1': 'placeId', '3': 3, '4': 1, '5': 5, '10': 'placeId'},
    const {'1': 'paymentType', '3': 4, '4': 1, '5': 14, '6': '.OrderPaymentType', '10': 'paymentType'},
    const {'1': 'phone', '3': 5, '4': 1, '5': 9, '10': 'phone'},
  ],
};

/// Descriptor for `OrderRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderRequestDescriptor = $convert.base64Decode('CgxPcmRlclJlcXVlc3QSJwoFaXRlbXMYASADKAsyES5PcmRlckl0ZW1SZXF1ZXN0UgVpdGVtcxIeCgpjdXN0b21lcklkGAIgASgFUgpjdXN0b21lcklkEhgKB3BsYWNlSWQYAyABKAVSB3BsYWNlSWQSMwoLcGF5bWVudFR5cGUYBCABKA4yES5PcmRlclBheW1lbnRUeXBlUgtwYXltZW50VHlwZRIUCgVwaG9uZRgFIAEoCVIFcGhvbmU=');
@$core.Deprecated('Use allOrdersResponseDescriptor instead')
const AllOrdersResponse$json = const {
  '1': 'AllOrdersResponse',
  '2': const [
    const {'1': 'oorders', '3': 1, '4': 3, '5': 11, '6': '.OrderRequest', '10': 'oorders'},
  ],
};

/// Descriptor for `AllOrdersResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List allOrdersResponseDescriptor = $convert.base64Decode('ChFBbGxPcmRlcnNSZXNwb25zZRInCgdvb3JkZXJzGAEgAygLMg0uT3JkZXJSZXF1ZXN0Ugdvb3JkZXJz');
@$core.Deprecated('Use orderResponseDescriptor instead')
const OrderResponse$json = const {
  '1': 'OrderResponse',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.OrderItemResponse', '10': 'items'},
    const {'1': 'createdAt', '3': 2, '4': 1, '5': 5, '10': 'createdAt'},
    const {'1': 'paymentType', '3': 3, '4': 1, '5': 14, '6': '.OrderPaymentType', '10': 'paymentType'},
    const {'1': 'status', '3': 4, '4': 1, '5': 14, '6': '.OrderStatus', '10': 'status'},
  ],
};

/// Descriptor for `OrderResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderResponseDescriptor = $convert.base64Decode('Cg1PcmRlclJlc3BvbnNlEigKBWl0ZW1zGAEgAygLMhIuT3JkZXJJdGVtUmVzcG9uc2VSBWl0ZW1zEhwKCWNyZWF0ZWRBdBgCIAEoBVIJY3JlYXRlZEF0EjMKC3BheW1lbnRUeXBlGAMgASgOMhEuT3JkZXJQYXltZW50VHlwZVILcGF5bWVudFR5cGUSJAoGc3RhdHVzGAQgASgOMgwuT3JkZXJTdGF0dXNSBnN0YXR1cw==');
@$core.Deprecated('Use orderItemRequestDescriptor instead')
const OrderItemRequest$json = const {
  '1': 'OrderItemRequest',
  '2': const [
    const {'1': 'itemId', '3': 1, '4': 1, '5': 5, '10': 'itemId'},
    const {'1': 'count', '3': 2, '4': 1, '5': 5, '10': 'count'},
  ],
};

/// Descriptor for `OrderItemRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderItemRequestDescriptor = $convert.base64Decode('ChBPcmRlckl0ZW1SZXF1ZXN0EhYKBml0ZW1JZBgBIAEoBVIGaXRlbUlkEhQKBWNvdW50GAIgASgFUgVjb3VudA==');
@$core.Deprecated('Use orderItemResponseDescriptor instead')
const OrderItemResponse$json = const {
  '1': 'OrderItemResponse',
  '2': const [
    const {'1': 'item', '3': 1, '4': 1, '5': 11, '6': '.ItemResponse', '10': 'item'},
    const {'1': 'count', '3': 2, '4': 1, '5': 5, '10': 'count'},
  ],
};

/// Descriptor for `OrderItemResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderItemResponseDescriptor = $convert.base64Decode('ChFPcmRlckl0ZW1SZXNwb25zZRIhCgRpdGVtGAEgASgLMg0uSXRlbVJlc3BvbnNlUgRpdGVtEhQKBWNvdW50GAIgASgFUgVjb3VudA==');
