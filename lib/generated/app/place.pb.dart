///
//  Generated code. Do not modify.
//  source: app/place.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../common/image.pb.dart' as $2;
import 'item_category.pb.dart' as $3;

class PlaceResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlaceResponse', createEmptyInstance: create)
    ..aOM<$2.ImageModel>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'image', subBuilder: $2.ImageModel.create)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'title')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deliveryCost', $pb.PbFieldType.O3, protoName: 'deliveryCost')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  PlaceResponse._() : super();
  factory PlaceResponse({
    $2.ImageModel? image,
    $core.String? title,
    $core.String? description,
    $core.int? deliveryCost,
    $core.int? id,
  }) {
    final _result = create();
    if (image != null) {
      _result.image = image;
    }
    if (title != null) {
      _result.title = title;
    }
    if (description != null) {
      _result.description = description;
    }
    if (deliveryCost != null) {
      _result.deliveryCost = deliveryCost;
    }
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory PlaceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlaceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlaceResponse clone() => PlaceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlaceResponse copyWith(void Function(PlaceResponse) updates) => super.copyWith((message) => updates(message as PlaceResponse)) as PlaceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlaceResponse create() => PlaceResponse._();
  PlaceResponse createEmptyInstance() => create();
  static $pb.PbList<PlaceResponse> createRepeated() => $pb.PbList<PlaceResponse>();
  @$core.pragma('dart2js:noInline')
  static PlaceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlaceResponse>(create);
  static PlaceResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $2.ImageModel get image => $_getN(0);
  @$pb.TagNumber(1)
  set image($2.ImageModel v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasImage() => $_has(0);
  @$pb.TagNumber(1)
  void clearImage() => clearField(1);
  @$pb.TagNumber(1)
  $2.ImageModel ensureImage() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get title => $_getSZ(1);
  @$pb.TagNumber(2)
  set title($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTitle() => $_has(1);
  @$pb.TagNumber(2)
  void clearTitle() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get description => $_getSZ(2);
  @$pb.TagNumber(3)
  set description($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDescription() => $_has(2);
  @$pb.TagNumber(3)
  void clearDescription() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get deliveryCost => $_getIZ(3);
  @$pb.TagNumber(4)
  set deliveryCost($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDeliveryCost() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeliveryCost() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get id => $_getIZ(4);
  @$pb.TagNumber(5)
  set id($core.int v) { $_setSignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasId() => $_has(4);
  @$pb.TagNumber(5)
  void clearId() => clearField(5);
}

class PlaceResponseCategories extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlaceResponseCategories', createEmptyInstance: create)
    ..pc<$3.ItemCategoryResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'categories', $pb.PbFieldType.PM, subBuilder: $3.ItemCategoryResponse.create)
    ..hasRequiredFields = false
  ;

  PlaceResponseCategories._() : super();
  factory PlaceResponseCategories({
    $core.Iterable<$3.ItemCategoryResponse>? categories,
  }) {
    final _result = create();
    if (categories != null) {
      _result.categories.addAll(categories);
    }
    return _result;
  }
  factory PlaceResponseCategories.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlaceResponseCategories.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlaceResponseCategories clone() => PlaceResponseCategories()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlaceResponseCategories copyWith(void Function(PlaceResponseCategories) updates) => super.copyWith((message) => updates(message as PlaceResponseCategories)) as PlaceResponseCategories; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlaceResponseCategories create() => PlaceResponseCategories._();
  PlaceResponseCategories createEmptyInstance() => create();
  static $pb.PbList<PlaceResponseCategories> createRepeated() => $pb.PbList<PlaceResponseCategories>();
  @$core.pragma('dart2js:noInline')
  static PlaceResponseCategories getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlaceResponseCategories>(create);
  static PlaceResponseCategories? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$3.ItemCategoryResponse> get categories => $_getList(0);
}

class PlaceRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlaceRequest', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'id', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  PlaceRequest._() : super();
  factory PlaceRequest({
    $core.int? id,
  }) {
    final _result = create();
    if (id != null) {
      _result.id = id;
    }
    return _result;
  }
  factory PlaceRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlaceRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlaceRequest clone() => PlaceRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlaceRequest copyWith(void Function(PlaceRequest) updates) => super.copyWith((message) => updates(message as PlaceRequest)) as PlaceRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlaceRequest create() => PlaceRequest._();
  PlaceRequest createEmptyInstance() => create();
  static $pb.PbList<PlaceRequest> createRepeated() => $pb.PbList<PlaceRequest>();
  @$core.pragma('dart2js:noInline')
  static PlaceRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlaceRequest>(create);
  static PlaceRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get id => $_getIZ(0);
  @$pb.TagNumber(1)
  set id($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
}

class PlaceDeliveryEstimationResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlaceDeliveryEstimationResponse', createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'from', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'to', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  PlaceDeliveryEstimationResponse._() : super();
  factory PlaceDeliveryEstimationResponse({
    $core.int? from,
    $core.int? to,
  }) {
    final _result = create();
    if (from != null) {
      _result.from = from;
    }
    if (to != null) {
      _result.to = to;
    }
    return _result;
  }
  factory PlaceDeliveryEstimationResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlaceDeliveryEstimationResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlaceDeliveryEstimationResponse clone() => PlaceDeliveryEstimationResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlaceDeliveryEstimationResponse copyWith(void Function(PlaceDeliveryEstimationResponse) updates) => super.copyWith((message) => updates(message as PlaceDeliveryEstimationResponse)) as PlaceDeliveryEstimationResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlaceDeliveryEstimationResponse create() => PlaceDeliveryEstimationResponse._();
  PlaceDeliveryEstimationResponse createEmptyInstance() => create();
  static $pb.PbList<PlaceDeliveryEstimationResponse> createRepeated() => $pb.PbList<PlaceDeliveryEstimationResponse>();
  @$core.pragma('dart2js:noInline')
  static PlaceDeliveryEstimationResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlaceDeliveryEstimationResponse>(create);
  static PlaceDeliveryEstimationResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get from => $_getIZ(0);
  @$pb.TagNumber(1)
  set from($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFrom() => $_has(0);
  @$pb.TagNumber(1)
  void clearFrom() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get to => $_getIZ(1);
  @$pb.TagNumber(2)
  set to($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTo() => $_has(1);
  @$pb.TagNumber(2)
  void clearTo() => clearField(2);
}

class PlacesDeliveryResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlacesDeliveryResponse', createEmptyInstance: create)
    ..pc<PlaceResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'restaurants', $pb.PbFieldType.PM, subBuilder: PlaceResponse.create)
    ..pc<PlaceResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'products', $pb.PbFieldType.PM, subBuilder: PlaceResponse.create)
    ..pc<PlaceResponse>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'zoo', $pb.PbFieldType.PM, subBuilder: PlaceResponse.create)
    ..hasRequiredFields = false
  ;

  PlacesDeliveryResponse._() : super();
  factory PlacesDeliveryResponse({
    $core.Iterable<PlaceResponse>? restaurants,
    $core.Iterable<PlaceResponse>? products,
    $core.Iterable<PlaceResponse>? zoo,
  }) {
    final _result = create();
    if (restaurants != null) {
      _result.restaurants.addAll(restaurants);
    }
    if (products != null) {
      _result.products.addAll(products);
    }
    if (zoo != null) {
      _result.zoo.addAll(zoo);
    }
    return _result;
  }
  factory PlacesDeliveryResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlacesDeliveryResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlacesDeliveryResponse clone() => PlacesDeliveryResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlacesDeliveryResponse copyWith(void Function(PlacesDeliveryResponse) updates) => super.copyWith((message) => updates(message as PlacesDeliveryResponse)) as PlacesDeliveryResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlacesDeliveryResponse create() => PlacesDeliveryResponse._();
  PlacesDeliveryResponse createEmptyInstance() => create();
  static $pb.PbList<PlacesDeliveryResponse> createRepeated() => $pb.PbList<PlacesDeliveryResponse>();
  @$core.pragma('dart2js:noInline')
  static PlacesDeliveryResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlacesDeliveryResponse>(create);
  static PlacesDeliveryResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PlaceResponse> get restaurants => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<PlaceResponse> get products => $_getList(1);

  @$pb.TagNumber(3)
  $core.List<PlaceResponse> get zoo => $_getList(2);
}

class PlacesServiceResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PlacesServiceResponse', createEmptyInstance: create)
    ..pc<PlaceResponse>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'repairElectronics', $pb.PbFieldType.PM, protoName: 'repairElectronics', subBuilder: PlaceResponse.create)
    ..pc<PlaceResponse>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'grooming', $pb.PbFieldType.PM, subBuilder: PlaceResponse.create)
    ..pc<PlaceResponse>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'beautySalons', $pb.PbFieldType.PM, protoName: 'beautySalons', subBuilder: PlaceResponse.create)
    ..hasRequiredFields = false
  ;

  PlacesServiceResponse._() : super();
  factory PlacesServiceResponse({
    $core.Iterable<PlaceResponse>? repairElectronics,
    $core.Iterable<PlaceResponse>? grooming,
    $core.Iterable<PlaceResponse>? beautySalons,
  }) {
    final _result = create();
    if (repairElectronics != null) {
      _result.repairElectronics.addAll(repairElectronics);
    }
    if (grooming != null) {
      _result.grooming.addAll(grooming);
    }
    if (beautySalons != null) {
      _result.beautySalons.addAll(beautySalons);
    }
    return _result;
  }
  factory PlacesServiceResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PlacesServiceResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PlacesServiceResponse clone() => PlacesServiceResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PlacesServiceResponse copyWith(void Function(PlacesServiceResponse) updates) => super.copyWith((message) => updates(message as PlacesServiceResponse)) as PlacesServiceResponse; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PlacesServiceResponse create() => PlacesServiceResponse._();
  PlacesServiceResponse createEmptyInstance() => create();
  static $pb.PbList<PlacesServiceResponse> createRepeated() => $pb.PbList<PlacesServiceResponse>();
  @$core.pragma('dart2js:noInline')
  static PlacesServiceResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PlacesServiceResponse>(create);
  static PlacesServiceResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PlaceResponse> get repairElectronics => $_getList(0);

  @$pb.TagNumber(2)
  $core.List<PlaceResponse> get grooming => $_getList(1);

  @$pb.TagNumber(3)
  $core.List<PlaceResponse> get beautySalons => $_getList(2);
}

