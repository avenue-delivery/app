///
//  Generated code. Do not modify.
//  source: app/place.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import '../common/empty.pb.dart' as $0;
import 'place.pb.dart' as $1;
export 'place.pb.dart';

class PlaceServiceClient extends $grpc.Client {
  static final _$getAllDeliveryPlaces =
      $grpc.ClientMethod<$0.Empty, $1.PlacesDeliveryResponse>(
          '/PlaceService/GetAllDeliveryPlaces',
          ($0.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.PlacesDeliveryResponse.fromBuffer(value));
  static final _$getAllServicePlaces =
      $grpc.ClientMethod<$0.Empty, $1.PlacesServiceResponse>(
          '/PlaceService/GetAllServicePlaces',
          ($0.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.PlacesServiceResponse.fromBuffer(value));
  static final _$getPlaceCategoriesWithItems =
      $grpc.ClientMethod<$1.PlaceRequest, $1.PlaceResponseCategories>(
          '/PlaceService/GetPlaceCategoriesWithItems',
          ($1.PlaceRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.PlaceResponseCategories.fromBuffer(value));

  PlaceServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.PlacesDeliveryResponse> getAllDeliveryPlaces(
      $0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllDeliveryPlaces, request, options: options);
  }

  $grpc.ResponseFuture<$1.PlacesServiceResponse> getAllServicePlaces(
      $0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllServicePlaces, request, options: options);
  }

  $grpc.ResponseFuture<$1.PlaceResponseCategories> getPlaceCategoriesWithItems(
      $1.PlaceRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getPlaceCategoriesWithItems, request,
        options: options);
  }
}

abstract class PlaceServiceBase extends $grpc.Service {
  $core.String get $name => 'PlaceService';

  PlaceServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.PlacesDeliveryResponse>(
        'GetAllDeliveryPlaces',
        getAllDeliveryPlaces_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.PlacesDeliveryResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.PlacesServiceResponse>(
        'GetAllServicePlaces',
        getAllServicePlaces_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.PlacesServiceResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.PlaceRequest, $1.PlaceResponseCategories>(
        'GetPlaceCategoriesWithItems',
        getPlaceCategoriesWithItems_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.PlaceRequest.fromBuffer(value),
        ($1.PlaceResponseCategories value) => value.writeToBuffer()));
  }

  $async.Future<$1.PlacesDeliveryResponse> getAllDeliveryPlaces_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return getAllDeliveryPlaces(call, await request);
  }

  $async.Future<$1.PlacesServiceResponse> getAllServicePlaces_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return getAllServicePlaces(call, await request);
  }

  $async.Future<$1.PlaceResponseCategories> getPlaceCategoriesWithItems_Pre(
      $grpc.ServiceCall call, $async.Future<$1.PlaceRequest> request) async {
    return getPlaceCategoriesWithItems(call, await request);
  }

  $async.Future<$1.PlacesDeliveryResponse> getAllDeliveryPlaces(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.PlacesServiceResponse> getAllServicePlaces(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.PlaceResponseCategories> getPlaceCategoriesWithItems(
      $grpc.ServiceCall call, $1.PlaceRequest request);
}
