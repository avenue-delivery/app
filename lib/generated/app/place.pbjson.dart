///
//  Generated code. Do not modify.
//  source: app/place.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,deprecated_member_use_from_same_package,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use placeResponseDescriptor instead')
const PlaceResponse$json = const {
  '1': 'PlaceResponse',
  '2': const [
    const {'1': 'image', '3': 1, '4': 1, '5': 11, '6': '.ImageModel', '10': 'image'},
    const {'1': 'title', '3': 2, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '9': 0, '10': 'description', '17': true},
    const {'1': 'deliveryCost', '3': 4, '4': 1, '5': 5, '10': 'deliveryCost'},
    const {'1': 'id', '3': 5, '4': 1, '5': 5, '10': 'id'},
  ],
  '8': const [
    const {'1': '_description'},
  ],
};

/// Descriptor for `PlaceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placeResponseDescriptor = $convert.base64Decode('Cg1QbGFjZVJlc3BvbnNlEiEKBWltYWdlGAEgASgLMgsuSW1hZ2VNb2RlbFIFaW1hZ2USFAoFdGl0bGUYAiABKAlSBXRpdGxlEiUKC2Rlc2NyaXB0aW9uGAMgASgJSABSC2Rlc2NyaXB0aW9uiAEBEiIKDGRlbGl2ZXJ5Q29zdBgEIAEoBVIMZGVsaXZlcnlDb3N0Eg4KAmlkGAUgASgFUgJpZEIOCgxfZGVzY3JpcHRpb24=');
@$core.Deprecated('Use placeResponseCategoriesDescriptor instead')
const PlaceResponseCategories$json = const {
  '1': 'PlaceResponseCategories',
  '2': const [
    const {'1': 'categories', '3': 1, '4': 3, '5': 11, '6': '.ItemCategoryResponse', '10': 'categories'},
  ],
};

/// Descriptor for `PlaceResponseCategories`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placeResponseCategoriesDescriptor = $convert.base64Decode('ChdQbGFjZVJlc3BvbnNlQ2F0ZWdvcmllcxI1CgpjYXRlZ29yaWVzGAEgAygLMhUuSXRlbUNhdGVnb3J5UmVzcG9uc2VSCmNhdGVnb3JpZXM=');
@$core.Deprecated('Use placeRequestDescriptor instead')
const PlaceRequest$json = const {
  '1': 'PlaceRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 5, '10': 'id'},
  ],
};

/// Descriptor for `PlaceRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placeRequestDescriptor = $convert.base64Decode('CgxQbGFjZVJlcXVlc3QSDgoCaWQYASABKAVSAmlk');
@$core.Deprecated('Use placeDeliveryEstimationResponseDescriptor instead')
const PlaceDeliveryEstimationResponse$json = const {
  '1': 'PlaceDeliveryEstimationResponse',
  '2': const [
    const {'1': 'from', '3': 1, '4': 1, '5': 5, '10': 'from'},
    const {'1': 'to', '3': 2, '4': 1, '5': 5, '10': 'to'},
  ],
};

/// Descriptor for `PlaceDeliveryEstimationResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placeDeliveryEstimationResponseDescriptor = $convert.base64Decode('Ch9QbGFjZURlbGl2ZXJ5RXN0aW1hdGlvblJlc3BvbnNlEhIKBGZyb20YASABKAVSBGZyb20SDgoCdG8YAiABKAVSAnRv');
@$core.Deprecated('Use placesDeliveryResponseDescriptor instead')
const PlacesDeliveryResponse$json = const {
  '1': 'PlacesDeliveryResponse',
  '2': const [
    const {'1': 'restaurants', '3': 1, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'restaurants'},
    const {'1': 'products', '3': 2, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'products'},
    const {'1': 'zoo', '3': 3, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'zoo'},
  ],
};

/// Descriptor for `PlacesDeliveryResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placesDeliveryResponseDescriptor = $convert.base64Decode('ChZQbGFjZXNEZWxpdmVyeVJlc3BvbnNlEjAKC3Jlc3RhdXJhbnRzGAEgAygLMg4uUGxhY2VSZXNwb25zZVILcmVzdGF1cmFudHMSKgoIcHJvZHVjdHMYAiADKAsyDi5QbGFjZVJlc3BvbnNlUghwcm9kdWN0cxIgCgN6b28YAyADKAsyDi5QbGFjZVJlc3BvbnNlUgN6b28=');
@$core.Deprecated('Use placesServiceResponseDescriptor instead')
const PlacesServiceResponse$json = const {
  '1': 'PlacesServiceResponse',
  '2': const [
    const {'1': 'repairElectronics', '3': 1, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'repairElectronics'},
    const {'1': 'grooming', '3': 2, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'grooming'},
    const {'1': 'beautySalons', '3': 3, '4': 3, '5': 11, '6': '.PlaceResponse', '10': 'beautySalons'},
  ],
};

/// Descriptor for `PlacesServiceResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List placesServiceResponseDescriptor = $convert.base64Decode('ChVQbGFjZXNTZXJ2aWNlUmVzcG9uc2USPAoRcmVwYWlyRWxlY3Ryb25pY3MYASADKAsyDi5QbGFjZVJlc3BvbnNlUhFyZXBhaXJFbGVjdHJvbmljcxIqCghncm9vbWluZxgCIAMoCzIOLlBsYWNlUmVzcG9uc2VSCGdyb29taW5nEjIKDGJlYXV0eVNhbG9ucxgDIAMoCzIOLlBsYWNlUmVzcG9uc2VSDGJlYXV0eVNhbG9ucw==');
