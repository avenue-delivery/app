///
//  Generated code. Do not modify.
//  source: common/image.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,constant_identifier_names,directives_ordering,library_prefixes,non_constant_identifier_names,prefer_final_fields,return_of_invalid_type,unnecessary_const,unnecessary_import,unnecessary_this,unused_import,unused_shown_name

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class ImageModel extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ImageModel', createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageUrl', protoName: 'imageUrl')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'imageBlurhash', protoName: 'imageBlurhash')
    ..hasRequiredFields = false
  ;

  ImageModel._() : super();
  factory ImageModel({
    $core.String? imageUrl,
    $core.String? imageBlurhash,
  }) {
    final _result = create();
    if (imageUrl != null) {
      _result.imageUrl = imageUrl;
    }
    if (imageBlurhash != null) {
      _result.imageBlurhash = imageBlurhash;
    }
    return _result;
  }
  factory ImageModel.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ImageModel.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ImageModel clone() => ImageModel()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ImageModel copyWith(void Function(ImageModel) updates) => super.copyWith((message) => updates(message as ImageModel)) as ImageModel; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ImageModel create() => ImageModel._();
  ImageModel createEmptyInstance() => create();
  static $pb.PbList<ImageModel> createRepeated() => $pb.PbList<ImageModel>();
  @$core.pragma('dart2js:noInline')
  static ImageModel getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ImageModel>(create);
  static ImageModel? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get imageUrl => $_getSZ(0);
  @$pb.TagNumber(1)
  set imageUrl($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasImageUrl() => $_has(0);
  @$pb.TagNumber(1)
  void clearImageUrl() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get imageBlurhash => $_getSZ(1);
  @$pb.TagNumber(2)
  set imageBlurhash($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasImageBlurhash() => $_has(1);
  @$pb.TagNumber(2)
  void clearImageBlurhash() => clearField(2);
}

