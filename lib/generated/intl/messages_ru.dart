// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "addBtn": MessageLookupByLibrary.simpleMessage("Добавить"),
        "cancelBtn": MessageLookupByLibrary.simpleMessage("Отмена"),
        "checkInternetConnectionText": MessageLookupByLibrary.simpleMessage(
            "Проверьте подключение к интернету и нажмите Повторить"),
        "continueBtn": MessageLookupByLibrary.simpleMessage("Продолжить"),
        "deleteAccountBtn":
            MessageLookupByLibrary.simpleMessage("Удалить аккаунт"),
        "deleteAccountTextBtn":
            MessageLookupByLibrary.simpleMessage("Удалить аккаунт"),
        "deleteAccountTitle": MessageLookupByLibrary.simpleMessage(
            "Действительно хотите удалить аккаунт? \nОтменить действие невозможно"),
        "deliveryTab": MessageLookupByLibrary.simpleMessage("Доставка"),
        "emptyCartTextBtn":
            MessageLookupByLibrary.simpleMessage("Очистить корзину"),
        "emptyCartTitle": MessageLookupByLibrary.simpleMessage(
            "Действительно хотите очистить корзину? \nОтменить действие невозможно"),
        "errorLoadShopsTitle": MessageLookupByLibrary.simpleMessage(
            "Не удалось получить заведения"),
        "errorText": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "exitAccountBtn":
            MessageLookupByLibrary.simpleMessage("Выйти из аккаунта"),
        "freeDelivery":
            MessageLookupByLibrary.simpleMessage("Бесплатная доставка"),
        "grammShort": MessageLookupByLibrary.simpleMessage("г."),
        "logoutBtn": MessageLookupByLibrary.simpleMessage("Выйти"),
        "logoutTitle": MessageLookupByLibrary.simpleMessage(
            "Действительно хотите Выйти из аккаунта?"),
        "notEnoughBtn": MessageLookupByLibrary.simpleMessage("Не хватает еще"),
        "orderFromBtn": MessageLookupByLibrary.simpleMessage("Заказ от"),
        "payBtn": MessageLookupByLibrary.simpleMessage("Оплатить"),
        "retryBtn": MessageLookupByLibrary.simpleMessage("Повторить"),
        "rubShort": MessageLookupByLibrary.simpleMessage("руб."),
        "rulesTextBtn": MessageLookupByLibrary.simpleMessage(
            "Правила пользования сервисом"),
        "saveBtn": MessageLookupByLibrary.simpleMessage("Сохранить"),
        "searchText":
            MessageLookupByLibrary.simpleMessage("Поиск по заведениям"),
        "sendBtn": MessageLookupByLibrary.simpleMessage("Отправить"),
        "servicesTab": MessageLookupByLibrary.simpleMessage("Услуги")
      };
}
