// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Продолжить`
  String get continueBtn {
    return Intl.message(
      'Продолжить',
      name: 'continueBtn',
      desc: '',
      args: [],
    );
  }

  /// `Отправить`
  String get sendBtn {
    return Intl.message(
      'Отправить',
      name: 'sendBtn',
      desc: '',
      args: [],
    );
  }

  /// `Сохранить`
  String get saveBtn {
    return Intl.message(
      'Сохранить',
      name: 'saveBtn',
      desc: '',
      args: [],
    );
  }

  /// `Оплатить`
  String get payBtn {
    return Intl.message(
      'Оплатить',
      name: 'payBtn',
      desc: '',
      args: [],
    );
  }

  /// `Заказ от`
  String get orderFromBtn {
    return Intl.message(
      'Заказ от',
      name: 'orderFromBtn',
      desc: '',
      args: [],
    );
  }

  /// `Не хватает еще`
  String get notEnoughBtn {
    return Intl.message(
      'Не хватает еще',
      name: 'notEnoughBtn',
      desc: '',
      args: [],
    );
  }

  /// `Выйти из аккаунта`
  String get exitAccountBtn {
    return Intl.message(
      'Выйти из аккаунта',
      name: 'exitAccountBtn',
      desc: '',
      args: [],
    );
  }

  /// `Добавить`
  String get addBtn {
    return Intl.message(
      'Добавить',
      name: 'addBtn',
      desc: '',
      args: [],
    );
  }

  /// `Повторить`
  String get retryBtn {
    return Intl.message(
      'Повторить',
      name: 'retryBtn',
      desc: '',
      args: [],
    );
  }

  /// `Правила пользования сервисом`
  String get rulesTextBtn {
    return Intl.message(
      'Правила пользования сервисом',
      name: 'rulesTextBtn',
      desc: '',
      args: [],
    );
  }

  /// `Удалить аккаунт`
  String get deleteAccountTextBtn {
    return Intl.message(
      'Удалить аккаунт',
      name: 'deleteAccountTextBtn',
      desc: '',
      args: [],
    );
  }

  /// `Очистить корзину`
  String get emptyCartTextBtn {
    return Intl.message(
      'Очистить корзину',
      name: 'emptyCartTextBtn',
      desc: '',
      args: [],
    );
  }

  /// `Действительно хотите очистить корзину? \nОтменить действие невозможно`
  String get emptyCartTitle {
    return Intl.message(
      'Действительно хотите очистить корзину? \nОтменить действие невозможно',
      name: 'emptyCartTitle',
      desc: '',
      args: [],
    );
  }

  /// `Отмена`
  String get cancelBtn {
    return Intl.message(
      'Отмена',
      name: 'cancelBtn',
      desc: '',
      args: [],
    );
  }

  /// `Действительно хотите Выйти из аккаунта?`
  String get logoutTitle {
    return Intl.message(
      'Действительно хотите Выйти из аккаунта?',
      name: 'logoutTitle',
      desc: '',
      args: [],
    );
  }

  /// `Выйти`
  String get logoutBtn {
    return Intl.message(
      'Выйти',
      name: 'logoutBtn',
      desc: '',
      args: [],
    );
  }

  /// `Действительно хотите удалить аккаунт? \nОтменить действие невозможно`
  String get deleteAccountTitle {
    return Intl.message(
      'Действительно хотите удалить аккаунт? \nОтменить действие невозможно',
      name: 'deleteAccountTitle',
      desc: '',
      args: [],
    );
  }

  /// `Удалить аккаунт`
  String get deleteAccountBtn {
    return Intl.message(
      'Удалить аккаунт',
      name: 'deleteAccountBtn',
      desc: '',
      args: [],
    );
  }

  /// `Доставка`
  String get deliveryTab {
    return Intl.message(
      'Доставка',
      name: 'deliveryTab',
      desc: '',
      args: [],
    );
  }

  /// `Услуги`
  String get servicesTab {
    return Intl.message(
      'Услуги',
      name: 'servicesTab',
      desc: '',
      args: [],
    );
  }

  /// `г.`
  String get grammShort {
    return Intl.message(
      'г.',
      name: 'grammShort',
      desc: '',
      args: [],
    );
  }

  /// `Поиск по заведениям`
  String get searchText {
    return Intl.message(
      'Поиск по заведениям',
      name: 'searchText',
      desc: '',
      args: [],
    );
  }

  /// `Бесплатная доставка`
  String get freeDelivery {
    return Intl.message(
      'Бесплатная доставка',
      name: 'freeDelivery',
      desc: '',
      args: [],
    );
  }

  /// `руб.`
  String get rubShort {
    return Intl.message(
      'руб.',
      name: 'rubShort',
      desc: '',
      args: [],
    );
  }

  /// `Не удалось получить заведения`
  String get errorLoadShopsTitle {
    return Intl.message(
      'Не удалось получить заведения',
      name: 'errorLoadShopsTitle',
      desc: '',
      args: [],
    );
  }

  /// `Проверьте подключение к интернету и нажмите Повторить`
  String get checkInternetConnectionText {
    return Intl.message(
      'Проверьте подключение к интернету и нажмите Повторить',
      name: 'checkInternetConnectionText',
      desc: '',
      args: [],
    );
  }

  /// `Ошибка`
  String get errorText {
    return Intl.message(
      'Ошибка',
      name: 'errorText',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
