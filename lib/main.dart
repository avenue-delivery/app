import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app/app.dart';
import 'di/common/di_scope_widget.dart';
import 'di/scopes/global/global.dart';
import 'ui/features/splash/splash_view.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    AsyncDependencyWidget(
      create: GlobalDependency.new,
      loaderBuilder: (context) => const SplashView(),
      child: const App(),
    ),
  );
}

void _onAppError(exception, stack) {
  // здесь можно использовать Crashlytics или аналог
  // FirebaseCrashlytics.instance.recordError(exception, stack);
}
