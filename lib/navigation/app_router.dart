import 'package:auto_route/annotations.dart';
import 'package:avenue_delivery/ui/features/place/place_view.dart';
import 'package:avenue_delivery/ui/features/splash/splash_view.dart';

import '../ui/features/home/home_view.dart';

@AdaptiveAutoRouter(
  preferRelativeImports: true,
  routes: <AutoRoute>[
    AutoRoute(
      page: HomeView,
      initial: true,
    ),
    AutoRoute(
      page: PlaceView,
    ),
    AutoRoute(
      page: SplashView,
      // initial: true,
    ),
  ],
)
class $AppRoutes {}
