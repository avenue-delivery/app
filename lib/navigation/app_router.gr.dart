// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;

import '../generated/app/place.pb.dart' as _i6;
import '../ui/features/home/home_view.dart' as _i1;
import '../ui/features/place/place_view.dart' as _i2;
import '../ui/features/splash/splash_view.dart' as _i3;

class AppRoutes extends _i4.RootStackRouter {
  AppRoutes([_i5.GlobalKey<_i5.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    HomeViewRoute.name: (routeData) {
      return _i4.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i1.HomeView(),
      );
    },
    PlaceViewRoute.name: (routeData) {
      final args = routeData.argsAs<PlaceViewRouteArgs>();
      return _i4.AdaptivePage<dynamic>(
        routeData: routeData,
        child: _i2.PlaceView(
          place: args.place,
          key: args.key,
        ),
      );
    },
    SplashViewRoute.name: (routeData) {
      return _i4.AdaptivePage<dynamic>(
        routeData: routeData,
        child: const _i3.SplashView(),
      );
    },
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(
          HomeViewRoute.name,
          path: '/',
        ),
        _i4.RouteConfig(
          PlaceViewRoute.name,
          path: '/place-view',
        ),
        _i4.RouteConfig(
          SplashViewRoute.name,
          path: '/splash-view',
        ),
      ];
}

/// generated route for
/// [_i1.HomeView]
class HomeViewRoute extends _i4.PageRouteInfo<void> {
  const HomeViewRoute()
      : super(
          HomeViewRoute.name,
          path: '/',
        );

  static const String name = 'HomeViewRoute';
}

/// generated route for
/// [_i2.PlaceView]
class PlaceViewRoute extends _i4.PageRouteInfo<PlaceViewRouteArgs> {
  PlaceViewRoute({
    required _i6.PlaceResponse place,
    _i5.Key? key,
  }) : super(
          PlaceViewRoute.name,
          path: '/place-view',
          args: PlaceViewRouteArgs(
            place: place,
            key: key,
          ),
        );

  static const String name = 'PlaceViewRoute';
}

class PlaceViewRouteArgs {
  const PlaceViewRouteArgs({
    required this.place,
    this.key,
  });

  final _i6.PlaceResponse place;

  final _i5.Key? key;

  @override
  String toString() {
    return 'PlaceViewRouteArgs{place: $place, key: $key}';
  }
}

/// generated route for
/// [_i3.SplashView]
class SplashViewRoute extends _i4.PageRouteInfo<void> {
  const SplashViewRoute()
      : super(
          SplashViewRoute.name,
          path: '/splash-view',
        );

  static const String name = 'SplashViewRoute';
}
