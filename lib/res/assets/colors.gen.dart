/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import

import 'package:flutter/painting.dart';
import 'package:flutter/material.dart';

class ColorName {
  ColorName._();

  /// Color: #828282
  static const Color additional1 = Color(0xFF828282);

  /// Color: #CBCBCB
  static const Color additional2 = Color(0xFFCBCBCB);

  /// Color: #F0F0F0
  static const Color additional3 = Color(0xFFF0F0F0);

  /// Color: #F3F3F3
  static const Color additional4 = Color(0xFFF3F3F3);

  /// Color: #2F3130
  static const Color black = Color(0xFF2F3130);

  /// Color: #C21B1B
  static const Color error = Color(0xFFC21B1B);

  /// Color: #207CE9
  static const Color links1 = Color(0xFF207CE9);

  /// Color: #4E9D3A
  static const Color links2 = Color(0xFF4E9D3A);

  /// Color: #E09A59
  static const Color primary = Color(0xFFE09A59);

  /// Color: #FFFFFF
  static const Color white = Color(0xFFFFFFFF);
}
