import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:flutter/material.dart';

class AppAltButton extends StatelessWidget {
  const AppAltButton({
    required this.child,
    this.onTap,
    super.key,
  });
  final VoidCallback? onTap;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: onTap,
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 32),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(13),
          color: onTap == null ? ColorName.additional2 : ColorName.white,
        ),
        alignment: Alignment.center,
        child: child,
      ),
    );
  }
}
