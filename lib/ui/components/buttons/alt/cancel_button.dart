import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/ui/components/buttons/alt/app_alt_button.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

class CancelButton extends StatelessWidget {
  const CancelButton({this.onTap, super.key});
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return AppAltButton(
      onTap: onTap,
      child: Text(
        S.of(context).cancelBtn,
        style: AppTypography.mainBtn,
      ),
    );
  }
}
