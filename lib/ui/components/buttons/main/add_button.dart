import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddButton extends StatelessWidget {
  const AddButton(
      {this.onTap,
      this.count = 0,
      super.key,
      required this.onAdd,
      required this.onRemove});
  final VoidCallback? onTap;
  final VoidCallback onAdd;
  final VoidCallback onRemove;
  final int count;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: count == 0 ? onAdd : null,
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          color: ColorName.primary,
          borderRadius: BorderRadius.circular(20),
        ),
        alignment: Alignment.center,
        child: Builder(
          builder: (_) {
            if (count == 0) {
              return Text(
                S.of(context).addBtn,
                style: AppTypography.mainBtnWhite,
              );
            } else {
              return Row(
                children: [
                  Expanded(
                    child: CupertinoButton(
                      padding: EdgeInsets.zero,
                      onPressed: onAdd,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: const [
                          SizedBox(width: 16),
                          Icon(
                            Icons.add,
                            size: 18,
                            color: ColorName.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text(
                    count.toString(),
                    style: AppTypography.h4dditional4,
                  ),
                  Expanded(
                    child: CupertinoButton(
                      padding: EdgeInsets.zero,
                      onPressed: onRemove,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: const [
                          Icon(
                            Icons.remove,
                            size: 18,
                            color: ColorName.white,
                          ),
                          SizedBox(width: 16),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
