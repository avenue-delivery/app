import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';
import 'app_button.dart';

class ContinueButton extends StatelessWidget {
  const ContinueButton({this.onTap, super.key});
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return AppButton(
      onTap: onTap,
      child: Text(
        S.of(context).continueBtn,
        style: AppTypography.mainBtnWhite,
      ),
    );
  }
}
