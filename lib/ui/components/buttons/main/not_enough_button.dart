import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import 'app_button.dart';

class NotEnoughButton extends StatelessWidget {
  const NotEnoughButton({this.onTap, this.sum = 0, this.need = 0, super.key});
  final VoidCallback? onTap;
  final int? sum;
  final int? need;

  @override
  Widget build(BuildContext context) {
    return AppButton(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '${S.of(context).orderFromBtn} $sum р.',
            style: AppTypography.mainBtnWhite,
          ),
          const SizedBox(height: 2),
          Text(
            '${S.of(context).notEnoughBtn} $need р.',
            style: AppTypography.mainBtnWhite,
          ),
        ],
      ),
    );
  }
}
