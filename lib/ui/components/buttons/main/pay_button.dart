import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import 'app_button.dart';


class PayButton extends StatelessWidget {
  const PayButton({this.onTap, this.sum = 0, super.key});
  final VoidCallback? onTap;
  final int? sum;

  @override
  Widget build(BuildContext context) {
    return AppButton(
      onTap: onTap,
      child: Text(
        '${S.of(context).payBtn} $sum р.',
        style: AppTypography.mainBtnWhite,
      ),
    );
  }
}
