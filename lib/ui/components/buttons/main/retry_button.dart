import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import 'app_button.dart';


class RetryButton extends StatelessWidget {
  const RetryButton({this.onTap, super.key});
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return AppButton(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Assets.icons.refresh.svg(),
          const SizedBox(width: 8),
          Text(
            S.of(context).retryBtn,
            style: AppTypography.mainBtnWhite,
          ),
        ],
      ),
    );
  }
}
