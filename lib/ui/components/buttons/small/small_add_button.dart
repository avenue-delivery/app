import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SmallAddButton extends StatefulWidget {
  const SmallAddButton(
      {required this.onAdd, required this.onRemove, this.count = 1, super.key});
  final int count;
  final VoidCallback onAdd;
  final VoidCallback onRemove;

  @override
  State<SmallAddButton> createState() => _SmallAddButtonState();
}

class _SmallAddButtonState extends State<SmallAddButton> {
  int get count => widget.count;
  VoidCallback get onAdd => widget.onAdd;
  VoidCallback get onRemove => widget.onRemove;

  @override
  Widget build(BuildContext context) {
    final child = Container(
      width: 152,
      height: 32,
      decoration: BoxDecoration(
        color: ColorName.primary,
        borderRadius: BorderRadius.circular(20),
      ),
      alignment: Alignment.center,
      child: Builder(builder: (_) {
        if (count == 0) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.add,
                size: 18,
                color: ColorName.white,
              ),
              const SizedBox(width: 8),
              Text(
                S.of(context).addBtn,
                style: AppTypography.h6Additional4,
              )
            ],
          );
        } else {
          return Row(
            children: [
              Expanded(
                child: CupertinoButton(
                  padding: EdgeInsets.zero,
                  onPressed: onAdd,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: const [
                      SizedBox(width: 16),
                      Icon(
                        Icons.add,
                        size: 18,
                        color: ColorName.white,
                      ),
                    ],
                  ),
                ),
              ),
              Text(
                count.toString(),
                style: AppTypography.h4dditional4,
              ),
              Expanded(
                child: CupertinoButton(
                  padding: EdgeInsets.zero,
                  onPressed: onRemove,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: const [
                      Icon(
                        Icons.remove,
                        size: 18,
                        color: ColorName.white,
                      ),
                      SizedBox(width: 16),
                    ],
                  ),
                ),
              ),
            ],
          );
        }
      }),
    );
    if (count == 0) {
      return Tappable(
        onTap: onAdd,
        child: child,
      );
    } else {
      return child;
    }
  }
}
