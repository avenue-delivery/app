import 'dart:io';

import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppTextButton extends StatelessWidget {
  const AppTextButton({
    required this.title,
    this.onTap,
    this.style = AppTypography.mainBtn,
    super.key,
  });
  final String title;
  final VoidCallback? onTap;
  final TextStyle? style;

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid || Platform.isFuchsia) {
      return TextButton(
        onPressed: onTap,
        style: TextButton.styleFrom(
          foregroundColor: style!.color!.withOpacity(.1),
        ),
        child: Text(
          title,
          style: style,
        ),
      );
    } else {
      return CupertinoButton(
        onPressed: onTap,
        child: Text(
          title,
          style: style,
        ),
      );
    }
  }
}
