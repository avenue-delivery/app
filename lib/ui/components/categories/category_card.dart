import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import '../tappable.dart';

class CategoryCard extends StatelessWidget {
  const CategoryCard(
      {required this.title,
      required this.image,
      this.onTap,
      this.chipText,
      super.key});
  final String title;
  final String? chipText;
  final Image image;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: onTap,
      child: Container(
        height: 103,
        decoration: BoxDecoration(
          color: ColorName.additional4,
          borderRadius: BorderRadius.circular(8),
        ),
        padding: const EdgeInsets.only(top: 7, left: 6, bottom: 13),
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (chipText != null)
                  Container(
                    width: 80,
                    height: 24,
                    decoration: BoxDecoration(
                      color: ColorName.primary,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      chipText!,
                      style: AppTypography.chipBtnWhite,
                    ),
                  )
                else
                  const SizedBox(),
                Text(
                  title,
                  style: AppTypography.h4Semi,
                ),
              ],
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 4),
                child: image,
              ),
            )
          ],
        ),
      ),
    );
  }
}
