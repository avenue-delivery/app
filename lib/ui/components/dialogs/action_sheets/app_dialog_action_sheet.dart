import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/buttons/text/app_text_button.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

class AppDialogActionSheet extends StatelessWidget {
  const AppDialogActionSheet({
    required this.title,
    required this.action,
    super.key,
  });
  final String title;
  final AppTextButton action;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(13),
        color: ColorName.white,
      ),
      child: Column(
        children: [
          const SizedBox(height: 15),
          Text(
            title,
            style: AppTypography.h6Additional1,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 20),
          action,
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
