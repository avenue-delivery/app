import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/ui/components/buttons/text/app_text_button.dart';
import 'package:avenue_delivery/ui/components/dialogs/action_sheets/app_dialog_action_sheet.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

class EmptyCartActionSheet extends AppDialogActionSheet {
  EmptyCartActionSheet({this.onTap, super.key})
      : super(
          title: S.current.emptyCartTitle,
          action: AppTextButton(
            title: S.current.emptyCartTextBtn,
            style: AppTypography.mainBtnError,
            onTap: onTap,
          ),
        );
  final VoidCallback? onTap;
}
