import 'package:avenue_delivery/app/app.dart';
import 'package:avenue_delivery/ui/components/buttons/alt/cancel_button.dart';
import 'package:flutter/cupertino.dart';

import 'action_sheets/app_dialog_action_sheet.dart';

class AppDialog extends StatelessWidget {
  const AppDialog({required this.actionSheet, this.onCancel, super.key});
  final AppDialogActionSheet actionSheet;
  final VoidCallback? onCancel;

  static show(
          {required AppDialogActionSheet actionSheet,
          VoidCallback? onCancel}) =>
      showCupertinoModalPopup(
          context: App.router.navigatorKey.currentContext!,
          builder: (_) => Padding(
            padding: const EdgeInsets.all(16),
            child: AppDialog(
                  actionSheet: actionSheet,
                  onCancel: onCancel,
                ),
          ));

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        actionSheet,
        const SizedBox(height: 12),
        CancelButton(
          onTap: onCancel ?? Navigator.of(context).pop,
        ),
      ],
    );
  }
}
