import 'package:avenue_delivery/generated/common/image.pb.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/buttons/small/small_add_button.dart';
import 'package:avenue_delivery/ui/components/image/app_image.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';

import '../../../generated/l10n.dart';

class DishGridCard extends StatefulWidget {
  const DishGridCard({
    required this.onAdd,
    required this.onRemove,
    required this.onTap,
    required this.title,
    required this.price,
    this.count = 0,
    this.image,
    this.weight,
    super.key,
  });
  final String title;
  final int? weight;
  final ImageModel? image;
  final int? count;
  final int price;
  final VoidCallback onAdd;
  final VoidCallback onTap;
  final VoidCallback onRemove;

  @override
  State<DishGridCard> createState() => _DishGridCardState();
}

class _DishGridCardState extends State<DishGridCard> {
  bool onTap = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: CupertinoButton(
            padding: EdgeInsets.zero,
            onPressed: widget.onTap,
            child: Container(
              padding:
                  const EdgeInsets.only(bottom: 12, top: 6, left: 8, right: 8),
              decoration: BoxDecoration(
                color: ColorName.additional4,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: AppImage(
                      image: widget.image,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    '${widget.price} ₽',
                    style: AppTypography.h4,
                    maxLines: 1,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    widget.title,
                    style: AppTypography.h5,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    '${widget.weight} ${S.of(context).grammShort}',
                    style: AppTypography.h6Additional1,
                    maxLines: 1,
                  ),
                  const SizedBox(height: 8),
                  const SizedBox(
                    height: 35,
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 16,
          left: 0,
          right: 0,
          child: Center(
            child: SmallAddButton(
                count: widget.count ?? 0,
                onAdd: widget.onAdd,
                onRemove: widget.onRemove),
          ),
        ),
      ],
    );
  }
}
