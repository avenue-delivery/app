import 'package:avenue_delivery/generated/common/image.pb.dart';
import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/buttons/small/small_add_button.dart';
import 'package:avenue_delivery/ui/components/image/app_image.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';

class DishHorizontalCard extends StatelessWidget {
  const DishHorizontalCard({
    required this.onAdd,
    required this.onRemove,
    required this.title,
    required this.price,
    required this.onTap,
    this.count = 0,
    this.image,
    this.weight,
    super.key,
  });
  final String title;
  final int? weight;
  final ImageModel? image;
  final int? count;
  final int price;
  final VoidCallback onAdd;
  final VoidCallback onTap;
  final VoidCallback onRemove;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: () {},
          child: Column(
            children: [
              SizedBox(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppImage(
                      image: image,
                      width: 100,
                      height: 100,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    const SizedBox(width: 6),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                title,
                                style: AppTypography.h5,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                '$weight ${S.of(context).grammShort}',
                                style: AppTypography.h6Additional1,
                                maxLines: 1,
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const SizedBox(width: 152),
                              Text(
                                '$price ₽',
                                style: AppTypography.h4,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Container(
                width: double.infinity,
                height: 2.5,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.5),
                    color: ColorName.white),
              )
            ],
          ),
        ),
        Positioned(
          bottom: 22.5,
          left: 106,
          child: SmallAddButton(onAdd: () {}, onRemove: () {}),
        )
      ],
    );
  }
}
