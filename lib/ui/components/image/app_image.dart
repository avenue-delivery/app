import 'package:avenue_delivery/generated/common/image.pb.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';

class AppImage extends StatefulWidget {
  const AppImage(
      {this.borderRadius, this.image, this.width, this.height, super.key});
  final ImageModel? image;
  final double? width;
  final double? height;
  final BorderRadius? borderRadius;

  @override
  State<AppImage> createState() => _AppImageState();
}

class _AppImageState extends State<AppImage> {
  BorderRadius? get borderRadius => widget.borderRadius;
  ImageModel? get image => widget.image;
  double? get width => widget.width;
  double? get height => widget.height;
  @override
  Widget build(BuildContext context) {
    if (image == null) return const SizedBox();
    return ClipRRect(
      borderRadius: borderRadius,
      child: CachedNetworkImage(
        imageUrl: image!.imageUrl,
        fit: BoxFit.cover,
        width: width,
        height: height,
        fadeInDuration: const Duration(microseconds: 0),
        fadeOutDuration: const Duration(milliseconds: 0),
        progressIndicatorBuilder: (context, url, progress) => BlurHash(
          hash: image!.imageBlurhash,
          curve: Curves.linear,
          duration: const Duration(milliseconds: 0),
        ),
      ),
    );
  }
}
