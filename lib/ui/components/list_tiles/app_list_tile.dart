import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import '../tappable.dart';

class AppListTile extends StatelessWidget {
  const AppListTile({
    required this.title,
    required this.tralling,
    this.onTap,
    super.key,
  });
  final String title;
  final Widget tralling;
  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: onTap,
      child: DecoratedBox(
        decoration: const BoxDecoration(color: Colors.transparent),
        child: Padding(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 18, bottom: 18),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: AppTypography.additionalBtn2,
              ),
              tralling
            ],
          ),
        ),
      ),
    );
  }
}
