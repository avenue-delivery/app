import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/list_tiles/app_list_tile.dart';
import 'package:flutter/cupertino.dart';

typedef SwitchCallback = void Function(bool);

class AppSwitchListTile extends StatelessWidget {
  const AppSwitchListTile({
    required this.title,
    this.value = false,
    this.onChanged,
    super.key,
  });
  final String title;
  final SwitchCallback? onChanged;
  final bool value;

  @override
  Widget build(BuildContext context) {
    return AppListTile(
      title: title,
      tralling: CupertinoSwitch(
        value: value,
        onChanged: onChanged,
        trackColor: ColorName.additional2,
        activeColor: ColorName.primary,
      ),
    );
  }
}
