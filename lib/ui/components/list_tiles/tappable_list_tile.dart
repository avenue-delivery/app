import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/ui/components/list_tiles/app_list_tile.dart';
import 'package:flutter/material.dart';

class TappableListTile extends StatelessWidget {
  const TappableListTile({
    required this.title,
    this.onTap,
    super.key,
  });
  final VoidCallback? onTap;
  final String title;

  @override
  Widget build(BuildContext context) {
    return AppListTile(
      title: title,
      onTap: onTap,
      tralling: Assets.icons.arrowRight.svg(),
    );
  }
}
