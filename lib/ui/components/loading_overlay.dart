import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay({
    required this.isBusy,
    required this.child,
    Key? key,
  }) : super(key: key);
  final bool isBusy;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    Widget body = Stack(
      children: [
        child,
        if (isBusy)
          Container(
            color: Colors.black.withOpacity(.1),
            alignment: Alignment.center,
            child: Container(
              height: 48,
              width: 48,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: const CupertinoActivityIndicator(
                radius: 16,
              ),
            ),
          ),
      ],
    );

    body = Directionality(
      textDirection: TextDirection.ltr,
      child: body,
    );

    if (MediaQuery.maybeOf(context) == null) {
      return MediaQuery.fromWindow(child: body);
    }
    return body;
  }
}
