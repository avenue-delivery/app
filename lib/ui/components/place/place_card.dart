import 'package:avenue_delivery/generated/common/image.pb.dart';
import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/image/app_image.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import '../tappable.dart';

class PlaceCard extends StatelessWidget {
  const PlaceCard({
    required this.image,
    required this.title,
    required this.onTap,
    this.freeDelivery = false,
    super.key,
  });
  final ImageModel image;
  final String title;
  final VoidCallback onTap;
  final bool freeDelivery;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AspectRatio(
            aspectRatio: 351 / 152,
            child:AppImage(
                width: double.infinity,
                image: image,
                borderRadius: BorderRadius.circular(8),
              ),
          ),
          const SizedBox(height: 8),
          SizedBox(
            height: 32,
            child: Text(
              title,
              style: AppTypography.h4,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              Assets.icons.accessTime.svg(),
              const SizedBox(width: 4),
              const Text(
                '40 мин.',
                style: AppTypography.h6Additional1,
              ),
              const SizedBox(width: 5),
              if (freeDelivery) ...[
                Center(
                  child: Container(
                    width: 4,
                    height: 4,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: ColorName.additional1,
                    ),
                  ),
                ),
                const SizedBox(width: 5),
                Text(
                  S.of(context).freeDelivery,
                  style: AppTypography.h6Additional1,
                ),
              ],
            ],
          )
        ],
      ),
    );
  }
}
