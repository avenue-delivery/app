import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'reactive_scaffold_with_view_model_builder.dart';
import 'reactive_scaffold_with_view_model_listner.dart';

class ReactiveScaffold<T extends BaseViewModel> extends StatelessWidget {
  const ReactiveScaffold({
    required this.builder,
    Key? key,
    this.appBar,
    this.drawer,
    this.bottomNavigationBar,
    this.bottomSheet,
    this.floatingActionButton,
    this.viewModelBuilder,
    this.onModelReady,
    this.scaffoldKey,
    this.endDrawer,
    this.backgroundColor,
    this.handleError = false,
  }) : super(key: key);

  final Key? scaffoldKey;
  final AppBar? appBar;
  final Drawer? drawer;
  final Drawer? endDrawer;
  final BottomNavigationBar? bottomNavigationBar;
  final BottomSheet? bottomSheet;
  final FloatingActionButton? floatingActionButton;
  final Color? backgroundColor;
  final bool? handleError;
  final Widget Function(
    BuildContext context,
    T viewModel,
    Widget? child,
  ) builder;
  final ValueChanged<T>? onModelReady;
  final T Function()? viewModelBuilder;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: appBar,
      drawer: drawer,
      endDrawer: endDrawer,
      bottomNavigationBar: bottomNavigationBar,
      bottomSheet: bottomSheet,
      floatingActionButton: floatingActionButton,
      backgroundColor: backgroundColor ?? ColorName.white,
      body: Column(
        children: [
          Expanded(
            child: Builder(
              builder: (context) {
                if (viewModelBuilder != null) {
                  return ReactiveViewModelBuilderScaffoldBody<T>(
                    viewModelBuilder!,
                    body: builder,
                    viewModelReady: onModelReady,
                    handleError: handleError,
                  );
                } else {
                  return ReactiveViewModelListenerScaffoldBody<T>(
                    body: builder,
                    handleError: handleError,
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
