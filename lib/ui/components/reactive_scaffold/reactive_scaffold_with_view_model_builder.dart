import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../loading_overlay.dart';

class ReactiveViewModelBuilderScaffoldBody<T extends BaseViewModel>
    extends ViewModelBuilderWidget<T> {
  const ReactiveViewModelBuilderScaffoldBody(
    this._viewModelBuilder, {
    required this.body,
    this.handleError,
    this.viewModelReady,
    Key? key,
  }) : super(key: key);

  final Widget Function(
    BuildContext context,
    T viewModel,
    Widget? child,
  ) body;
  final ValueChanged<T>? viewModelReady;
  final T Function() _viewModelBuilder;
  final bool? handleError;

  @override
  Widget builder(BuildContext context, T viewModel, Widget? child) {
    if (viewModel.hasError && (handleError ?? false)) {
      // TODO(Kit): add error screen
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(Icons.error_outline),
            const SizedBox(height: 12),
            Text(viewModel.modelError.toString()),
            const SizedBox(height: 12),
          ],
        ),
      );
    } else {
      return LoadingOverlay(
        isBusy: viewModel.isBusy,
        child: body(context, viewModel, null),
      );
    }
  }

  @override
  void onViewModelReady(T viewModel) => viewModelReady?.call(viewModel);

  @override
  T viewModelBuilder(BuildContext context) => _viewModelBuilder();
}
