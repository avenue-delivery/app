import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

import '../loading_overlay.dart';


class ReactiveViewModelListenerScaffoldBody<T extends BaseViewModel>
    extends StatelessWidget {
  const ReactiveViewModelListenerScaffoldBody({
    required this.body,
    Key? key,
    this.handleError,
  }) : super(key: key);

  final Widget Function(
    BuildContext context,
    T viewModel,
    Widget? child,
  ) body;

  final bool? handleError;

  @override
  Widget build(BuildContext context) {
    final viewModel = context.watch<T>();
    if (viewModel.hasError && (handleError ?? false)) {
      // TODO(Kit): add error screen
      return Center(
        child: Column(
          children: [
            const Icon(Icons.error_outline),
            const SizedBox(height: 12),
            Text(viewModel.modelError.toString()),
          ],
        ),
      );
    } else {
      return LoadingOverlay(
        isBusy: viewModel.isBusy,
        child: body(context, viewModel, null),
      );
    }
  }
}
