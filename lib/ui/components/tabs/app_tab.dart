import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';

import '../tappable.dart';

class AppTab extends StatelessWidget {
  const AppTab(
      {required this.text, this.onTap, this.isActive = false, super.key});
  final bool isActive;
  final VoidCallback? onTap;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Tappable(
      onTap: onTap,
      child: Container(
        width: isActive ? 100 : 80,
        height: 32,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: isActive ? ColorName.black : Colors.transparent,
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: isActive ? AppTypography.mainBtnWhite : AppTypography.mainBtn,
        ),
      ),
    );
  }
}
