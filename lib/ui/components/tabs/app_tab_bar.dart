import 'package:flutter/material.dart';

import 'app_tab.dart';

typedef AppTabbarCallback = void Function(int);

class AppTabBar extends StatelessWidget {
  const AppTabBar(
      {required this.tabs, this.onChanged, this.index = 0, super.key})
      : assert(tabs.length > 1, 'Табов должно быть больше одного'),
        assert(
            index < tabs.length, 'Индекс не должен превышать количество табов');
  final List<String> tabs;
  final int index;
  final AppTabbarCallback? onChanged;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(
        tabs.length,
        (index) => AppTab(
          isActive: this.index == index,
          text: tabs[index],
          onTap: () => onChanged?.call(index),
        ),
      ),
    );
  }
}
