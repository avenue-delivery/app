import 'package:avenue_delivery/ui/components/zoom_tap_animation.dart';
import 'package:flutter/material.dart';

class Tappable extends StatelessWidget {
  const Tappable({
    required this.child,
    this.onTap,
    this.behavior,
    super.key,
  });
  final VoidCallback? onTap;
  final Widget child;
  final HitTestBehavior? behavior;

  @override
  Widget build(BuildContext context) {
    if (onTap == null) return child;
    return ZoomTapAnimation(
      onTap: onTap,
      hitTestBehavior: behavior,
      begin: 1,
      end: .95,
      beginCurve: Curves.easeIn,
      endCurve: Curves.easeOut,
      child: child,
    );
  }
}
