import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class AppTextField extends StatelessWidget {
  const AppTextField({
    this.controller,
    this.label,
    this.error,
    super.key,
  });
  final TextEditingController? controller;
  final String? error;
  final String? label;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 14),
      decoration: BoxDecoration(
        border: Border.all(
          color: error == null ? ColorName.black : ColorName.error,
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: TextField(
        controller: controller,
        cursorColor: ColorName.primary,
        decoration: InputDecoration(
          border: InputBorder.none,
          labelText: error ?? label,
          labelStyle: error == null
              ? AppTypography.textBigAdditional1
              : AppTypography.textBigError,
        ),
      ),
    );
  }
}
