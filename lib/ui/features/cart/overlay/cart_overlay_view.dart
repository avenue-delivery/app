import 'package:avenue_delivery/di/scopes/global/global.dart';
import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'cart_overlay_view_model.dart';

class CartOverlayView extends StatelessWidget {
  const CartOverlayView({super.key});

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder.reactive(
      viewModelBuilder: () => CartOverlayViewModel(context.global.cartService),
      builder: ((context, model, child) {
        return SizedBox(
          height: 50,
          width: 180,
          child: Stack(
            children: [
              AnimatedPositioned(
                right: model.show ? -3 : -180,
                curve: Curves.easeInOut,
                duration: const Duration(milliseconds: 300),
                child: AnimatedOpacity(
                  opacity: model.show ? 1 : 0,
                  curve: Curves.easeInOut,
                  duration: const Duration(milliseconds: 300),
                  child: Material(
                    color: Colors.transparent,
                    child: Container(
                      width: 180,
                      height: 50,
                      decoration: const BoxDecoration(
                        color: ColorName.primary,
                        borderRadius: BorderRadius.horizontal(
                          left: Radius.circular(45),
                        ),
                        border: Border.fromBorderSide(
                          BorderSide(color: ColorName.white, width: 3),
                        ),
                      ),
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Assets.icons.cart.svg(),
                          const SizedBox(
                            width: 4,
                          ),
                          if (model.show)
                            Text(
                              '${model.inCart} ${S.of(context).rubShort}',
                              style: AppTypography.additionalBtn1White,
                            )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
