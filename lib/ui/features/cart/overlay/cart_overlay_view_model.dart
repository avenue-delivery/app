import 'package:avenue_delivery/domain/cart/cart_service.dart';
import 'package:stacked/stacked.dart';

class CartOverlayViewModel extends ReactiveViewModel {
  CartOverlayViewModel(this._cartService);
  final CartService _cartService;

  int get inCart => _cartService.inCart;
  bool get show => _cartService.items.isNotEmpty;

  @override
  List<ReactiveServiceMixin> get reactiveServices => [_cartService];
}
