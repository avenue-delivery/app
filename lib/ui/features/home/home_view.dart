import 'package:avenue_delivery/app/app.dart';
import 'package:avenue_delivery/di/scopes/global/global.dart';
import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/navigation/app_router.gr.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/categories/category_card.dart';
import 'package:avenue_delivery/ui/components/place/place_card.dart';
import 'package:avenue_delivery/ui/components/reactive_scaffold/reactive_scaffold.dart';
import 'package:avenue_delivery/ui/components/tabs/app_tab_bar.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import './home_view_model.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: ReactiveScaffold<HomeViewModel>(
        viewModelBuilder: () => HomeViewModel(
          context.global.placesService,
        ),
        onModelReady: (HomeViewModel model) async {
          await model.init();
        },
        builder: (
          BuildContext context,
          HomeViewModel model,
          Widget? child,
        ) {
          return Scaffold(
            body: SafeArea(
              bottom: false,
              child: CustomScrollView(
                slivers: [
                  SliverAppBar(
                    systemOverlayStyle: SystemUiOverlayStyle.dark,
                    floating: true,
                    collapsedHeight: 72,
                    backgroundColor: ColorName.white,
                    surfaceTintColor: ColorName.white,
                    flexibleSpace: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Column(
                        children: [
                          const SizedBox(height: 4),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Tappable(
                                child: Assets.icons.message.svg(),
                              ),
                              AppTabBar(
                                tabs: [
                                  S.of(context).deliveryTab,
                                  S.of(context).servicesTab,
                                ],
                              ),
                              Tappable(
                                child: Assets.icons.account.svg(),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16),
                          CupertinoButton(
                            padding: EdgeInsets.zero,
                            minSize: 0,
                            onPressed: () {},
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  'Плесецкая 4, кв. 599',
                                  style: AppTypography.additionalBtn1,
                                ),
                                const SizedBox(width: 5),
                                Assets.icons.chevronBottom.svg(
                                  height: 16,
                                  width: 16,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SliverAppBar(
                    systemOverlayStyle: SystemUiOverlayStyle.dark,
                    backgroundColor: ColorName.white,
                    surfaceTintColor: ColorName.white,
                    toolbarHeight: 74,
                    expandedHeight: 74,
                    collapsedHeight: 74,
                    pinned: true,
                    flexibleSpace: Padding(
                      padding: const EdgeInsets.all(12),
                      child: TextField(
                        style: AppTypography.h4,
                        decoration: InputDecoration(
                          prefixIcon: SizedBox(
                            height: 24,
                            width: 24,
                            child: Center(
                              child: Assets.icons.searchNotFilled.svg(
                                height: 19.33,
                                width: 16.14,
                              ),
                            ),
                          ),
                          filled: true,
                          fillColor: ColorName.additional3,
                          labelText: S.of(context).searchText,
                          labelStyle: AppTypography.h4dditional1,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(22),
                            borderSide: BorderSide.none,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SliverAppBar(
                    systemOverlayStyle: SystemUiOverlayStyle.dark,
                    expandedHeight: 246,
                    collapsedHeight: 246,
                    toolbarHeight: 246,
                    primary: false,
                    titleSpacing: 0,
                    title: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: CategoryCard(
                                      chipText: 'от 25 мин.',
                                      onTap: () {},
                                      title: 'Рестораны',
                                      image: Assets.images.rest.image(),
                                    ),
                                  ),
                                  const SizedBox(width: 12),
                                  Expanded(
                                    child: CategoryCard(
                                      chipText: 'от 25 мин.',
                                      onTap: () {},
                                      title: 'Продукты',
                                      image: Assets.images.products.image(),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 8),
                              CategoryCard(
                                chipText: 'от 25 мин.',
                                onTap: () {},
                                title: 'Зоотовары',
                                image: Assets.images.zoo.image(),
                              ),
                              const SizedBox(height: 16),
                            ],
                          ),
                        ),
                        Container(
                          height: 12,
                          width: double.infinity,
                          color: ColorName.additional4,
                        ),
                      ],
                    ),
                  ),
                  if (model.hasError)
                    SliverPadding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 12),
                      sliver: SliverToBoxAdapter(
                          child: CupertinoAlertDialog(
                        title: Text(S.of(context).errorLoadShopsTitle),
                        content: Text(S.of(context).checkInternetConnectionText),
                        actions: [
                          CupertinoDialogAction(
                            onPressed: model.init,
                            child: Text(S.of(context).retryBtn),
                          )
                        ],
                      )),
                    ),
                  SliverPadding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 12),
                    sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                        ((context, index) {
                          final place =
                              model.placesDelivery?.restaurants[index];
                          if (place == null) return const SizedBox();
                          return Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: PlaceCard(
                              freeDelivery: place.deliveryCost == 0,
                              image: place.image,
                              title: place.title,
                              onTap: () {
                                App.router.push(
                                  PlaceViewRoute(place: place),
                                );
                              },
                            ),
                          );
                        }),
                        childCount:
                            model.placesDelivery?.restaurants.length ?? 0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
