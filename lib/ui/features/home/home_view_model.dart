import 'package:avenue_delivery/domain/places/places_service.dart';
import 'package:avenue_delivery/generated/app/place.pb.dart';
import 'package:stacked/stacked.dart';

class HomeViewModel extends BaseViewModel {
  HomeViewModel(
    this._placesService,
  );
  final PlacesService _placesService;

  PlacesDeliveryResponse? get placesDelivery =>
      _placesService.placesDelivery.value;
  PlacesServiceResponse? get placesService =>
      _placesService.placesService.value;

  Future<void> init() async {
    fetchDelivery();
    fetchService();
  }


  Future<void> fetchDelivery() => runBusyFuture(_placesService.fetchDelivery());
  Future<void> fetchService() => runBusyFuture(_placesService.fetchService());
}
