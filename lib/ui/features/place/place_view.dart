import 'dart:developer' as d;
import 'dart:math';
import 'dart:ui';

import 'package:avenue_delivery/app/app.dart';
import 'package:avenue_delivery/di/scopes/global/global.dart';
import 'package:avenue_delivery/generated/app/item_category.pb.dart';
import 'package:avenue_delivery/generated/app/place.pb.dart';
import 'package:avenue_delivery/generated/l10n.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/chips/app_chip.dart';
import 'package:avenue_delivery/ui/components/dish/dish_grid_card.dart';
import 'package:avenue_delivery/ui/components/image/app_image.dart';
import 'package:avenue_delivery/ui/components/reactive_scaffold/reactive_scaffold.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:avenue_delivery/ui/features/product/product_view.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:blur/blur.dart';
import 'package:flutter/material.dart';

import './place_view_model.dart';

class PlaceView extends StatelessWidget {
  const PlaceView({required this.place, super.key});
  final PlaceResponse place;

  @override
  Widget build(BuildContext context) {
    return ReactiveScaffold<PlaceViewModel>(
      viewModelBuilder: () => PlaceViewModel(
        context.global.placesService,
        context.global.cartService,
        placeId: place.id,
      ),
      onModelReady: (PlaceViewModel model) async {
        await model.init();
      },
      builder: (
        BuildContext context,
        PlaceViewModel model,
        Widget? child,
      ) {
        return CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              delegate: PlaceAppBarDeligate(
                place: place,
                categories: model.categories,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.only(
                  left: 8,
                  right: 8,
                  bottom: 72 + MediaQuery.of(context).viewPadding.bottom),
              sliver: SliverGrid.count(
                crossAxisCount: 2,
                childAspectRatio: 176 / 271,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
                children: List.generate(model.items.length, (index) {
                  final item = model.items[index];
                  return DishGridCard(
                    onAdd: () => model.addItemToCart(item.id, item.price),
                    onRemove: () =>
                        model.removeItemFromCart(item.id, item.price),
                    onTap: () => showProductBottomSheet(item),
                    count: model.itemsInCart(item.id),
                    title: item.title,
                    price: item.price,
                    image: item.image,
                    weight: item.weight,
                  );
                }),
              ),
            )
          ],
        );
      },
    );
  }
}

class PlaceAppBarDeligate extends SliverPersistentHeaderDelegate {
  PlaceAppBarDeligate({
    required this.place,
    required this.categories,
  });
  final PlaceResponse place;
  final List<ItemCategoryResponse> categories;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final step = min(
      1,
      max(
        0,
        shrinkOffset / (MediaQuery.of(context).viewPadding.top + 130),
      ),
    ).toDouble();
    return SizedBox(
      height: maxExtent,
      child: Blur(
        blur: 7,
        colorOpacity: .85,
        overlay: Column(
          children: [
            Expanded(
              child: SizedBox(
                height: MediaQuery.of(context).viewPadding.top + 235,
                child: Stack(
                  children: [
                    Opacity(
                      opacity: Curves.easeOut.transform(
                        max(0, .4 * (1 - step)),
                      ),
                      child: AppImage(
                        width: double.infinity,
                        height: maxExtent,
                        image: place.image,
                        borderRadius: BorderRadius.circular(
                          16 *
                              (1 -
                                  Curves.easeOut.transform(
                                    step,
                                  )),
                        ),
                      ),
                    ),
                    Positioned(
                      top: lerpDouble(
                          78 + MediaQuery.of(context).viewPadding.top,
                          MediaQuery.of(context).viewPadding.top + 16,
                          step),
                      left: 20 + 52 * Curves.easeOut.transform(step),
                      right: 20,
                      child: Text(
                        place.title,
                        style: AppTypography.h4.copyWith(
                          fontSize: lerpDouble(32, 20, step),
                        ),
                        maxLines: lerpDouble(3, 1, step)!.toInt(),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Positioned(
                      left: 0,
                      right: 0,
                      bottom: 27,
                      child: Opacity(
                        opacity: Curves.easeOut.transform(
                          max(0, 1 - step * 3.5),
                        ),
                        child: Container(
                          decoration: const BoxDecoration(
                            color: ColorName.white,
                          ),
                          height: 24,
                          padding: const EdgeInsets.symmetric(horizontal: 23),
                          child: Row(
                            children: [
                              const Text(
                                '40 мин.',
                                style: AppTypography.h6,
                              ),
                              const SizedBox(width: 5),
                              Center(
                                child: Container(
                                  width: 4,
                                  height: 4,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: ColorName.additional1,
                                  ),
                                ),
                              ),
                              const SizedBox(width: 5),
                              Text(
                                place.deliveryCost == 0
                                    ? S.of(context).freeDelivery
                                    : place.deliveryCost.toString(),
                                style: AppTypography.h6,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 12,
                      top: MediaQuery.of(context).viewPadding.top + 12,
                      child: Tappable(
                          onTap: Navigator.of(context).pop,
                          child: Assets.icons.arrowLeftBtn.svg()),
                    ),
                    Positioned(
                      right: 12,
                      top: MediaQuery.of(context).viewPadding.top + 12,
                      child: Assets.icons.search.svg(),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 56,
              child: ListView(
                padding: const EdgeInsets.only(
                    top: 12, left: 8, right: 8, bottom: 12),
                scrollDirection: Axis.horizontal,
                children: List.generate(
                    categories.length,
                    (index) => Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: AppChip(
                            text: categories[index].title,
                            isActive: index.isEven,
                          ),
                        )),
              ),
            ),
          ],
        ),
        child: const SizedBox(),
      ),
    );
  }

  @override
  double get maxExtent =>
      MediaQuery.of(App.router.navigatorKey.currentContext!).viewPadding.top +
      291;

  @override
  double get minExtent =>
      MediaQuery.of(App.router.navigatorKey.currentContext!).viewPadding.top +
      108;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
