import 'package:avenue_delivery/domain/cart/cart_service.dart';
import 'package:avenue_delivery/domain/places/places_service.dart';
import 'package:avenue_delivery/generated/app/item.pb.dart';
import 'package:avenue_delivery/generated/app/item_category.pb.dart';
import 'package:stacked/stacked.dart';

class PlaceViewModel extends ReactiveViewModel {
  PlaceViewModel(
    this._placesService,
    this._cartService, {
    required this.placeId,
  });
  final PlacesService _placesService;
  final CartService _cartService;
  final int placeId;

  List<ItemCategoryResponse> categories = [];
  List<ItemResponse> items = [];

  Future<void> init() async {
    runBusyFuture(fetch());
  }

  int itemsInCart(int id) => _cartService.itemsInCart(id);

  void addItemToCart(int id, int price) => _cartService.addToCart(id, price);

  void removeItemFromCart(int id, int price) =>
      _cartService.removeFromCart(id, price);

  Future<void> fetch() async {
    categories.clear();
    items.clear();
    final res = await _placesService.fetchPlaceCategories(placeId);
    categories.addAll(res);
    for (final c in categories) {
      items.addAll(c.items);
    }
  }

  @override
  List<ReactiveServiceMixin> get reactiveServices =>
      [_placesService, _cartService];
}
