import 'package:avenue_delivery/app/app.dart';
import 'package:avenue_delivery/di/scopes/global/global.dart';
import 'package:avenue_delivery/generated/app/item.pb.dart';
import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:avenue_delivery/ui/components/buttons/main/add_button.dart';
import 'package:avenue_delivery/ui/components/image/app_image.dart';
import 'package:avenue_delivery/ui/components/tappable.dart';
import 'package:avenue_delivery/ui/theme/typgraphy/app_typography.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import './product_view_model.dart';

Future<void> showProductBottomSheet(ItemResponse product) async =>
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      elevation: 0,
      context: App.router.navigatorKey.currentContext!,
      builder: (_) => ProductView(product: product),
    );

class ProductView extends StatelessWidget {
  const ProductView({
    required this.product,
    super.key,
  });
  final ItemResponse product;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProductViewModel>.reactive(
      viewModelBuilder: () => ProductViewModel(
        context.global.cartService,
      ),
      builder: (
        BuildContext context,
        ProductViewModel model,
        Widget? child,
      ) {
        return Column(
          children: [
            const SafeArea(
              child: SizedBox(
                height: 55,
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: ColorName.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(24),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 14),
                      Center(
                        child: Container(
                          height: 5,
                          width: 100,
                          decoration: BoxDecoration(
                              color: ColorName.additional2,
                              borderRadius: BorderRadius.circular(100)),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Tappable(
                              onTap: App.router.pop,
                              child: Assets.icons.close.svg()),
                          const SizedBox(width: 5),
                        ],
                      ),
                      const SizedBox(height: 21),
                      AppImage(
                        image: product.image,
                        borderRadius: BorderRadius.circular(24),
                        width: double.infinity,
                        height: 220,
                      ),
                      const SizedBox(height: 12),
                      Text(
                        product.title,
                        style: AppTypography.h2,
                      ),
                      const SizedBox(height: 12),
                      Center(
                        child: Container(
                          height: 2.5,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color: ColorName.additional3,
                              borderRadius: BorderRadius.circular(100)),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        product.title,
                        style: AppTypography.h4dditional1,
                      ),
                      const SizedBox(height: 10),
                      Text(
                        product.description,
                        style: AppTypography.textBig,
                      ),
                      const Spacer(),
                      SafeArea(
                        top: false,
                        child: AddButton(
                          count: model.itemsInCart(product.id),
                          onAdd: () =>
                              model.addItemToCart(product.id, product.price),
                          onRemove: () => model.removeItemFromCart(
                              product.id, product.price),
                        ),
                      ),
                      const SizedBox(height: 12),
                      AnimatedContainer(
                        height: model.isCartEmpty ? 0 : 64,
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.easeIn,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
