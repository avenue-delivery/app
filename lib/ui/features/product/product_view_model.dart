import 'package:avenue_delivery/domain/cart/cart_service.dart';
import 'package:stacked/stacked.dart';

class ProductViewModel extends ReactiveViewModel {
  ProductViewModel(this._cartService);
  final CartService _cartService;

  int itemsInCart(int id) => _cartService.itemsInCart(id);

  bool get isCartEmpty => _cartService.items.isEmpty;

  void addItemToCart(int id, int price) => _cartService.addToCart(id, price);

  void removeItemFromCart(int id, int price) =>
      _cartService.removeFromCart(id, price);

  @override
  List<ReactiveServiceMixin> get reactiveServices => [_cartService];
}
