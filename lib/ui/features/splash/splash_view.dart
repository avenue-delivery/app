import 'package:avenue_delivery/res/assets/assets.gen.dart';
import 'package:flutter/material.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget body = Scaffold(
      body: Center(
        child: Assets.icons.logo.svg(),
      ),
    );

    body = Directionality(
      textDirection: TextDirection.ltr,
      child: body,
    );

    if (MediaQuery.maybeOf(context) == null) {
      return MediaQuery.fromWindow(child: body);
    }
    return body;
  }
}
