import 'package:avenue_delivery/res/assets/colors.gen.dart';
import 'package:flutter/material.dart';

class AppTypography {
  static const h1 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w600,
    color: ColorName.black,
  );
  static const h2 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: ColorName.black,
  );
  static const h3 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: ColorName.black,
  );
  static const h4 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: ColorName.black,
  );
  static const h4Semi = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
    color: ColorName.black,
  );
  static const h4dditional4 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: ColorName.additional4,
  );
  static const h4dditional1 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: ColorName.additional1,
  );
  static const h5 = TextStyle(
    fontSize: 14,
    color: ColorName.black,
  );
  static const h6 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    color: ColorName.black,
  );
  static const h6Additional1 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    color: ColorName.additional1,
  );
  static const h6Additional4 = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    color: ColorName.additional4,
  );
  static const mainBtn = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: ColorName.black,
  );
  static const mainBtnLinks1 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: ColorName.links1,
  );
  static const mainBtnError = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: ColorName.error,
  );
  static const mainBtnWhite = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: ColorName.white,
  );
  static const additionalBtn1 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: ColorName.black,
  );
  static const additionalBtn1White = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: ColorName.white,
  );
  static const additionalBtn2 = TextStyle(
    fontSize: 16,
    color: ColorName.black,
  );
  static const fieldSmallHintBtn = TextStyle(
    fontSize: 11,
    color: ColorName.black,
  );
  static const chipBtnWhite = TextStyle(
    fontSize: 12,
    color: ColorName.white,
    letterSpacing: -.001,
  );
  static const fieldSmallHintBtnWhite = TextStyle(
    fontSize: 11,
    color: ColorName.white,
  );
  static const textSmall = TextStyle(
    fontSize: 14,
    color: ColorName.black,
  );
  static const textBig = TextStyle(
    fontSize: 16,
    color: ColorName.black,
  );
  static const textBigAdditional1 = TextStyle(
    fontSize: 16,
    color: ColorName.additional1,
  );
  static const textBigError = TextStyle(
    fontSize: 16,
    color: ColorName.error,
  );
}
